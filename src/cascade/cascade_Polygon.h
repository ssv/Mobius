//-----------------------------------------------------------------------------
// Created on: 08 February 2025
//-----------------------------------------------------------------------------
// Copyright (c) 2025-present, Sergey Slyadnev
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
//    * Redistributions of source code must retain the above copyright
//      notice, this list of conditions and the following disclaimer.
//    * Redistributions in binary form must reproduce the above copyright
//      notice, this list of conditions and the following disclaimer in the
//      documentation and/or other materials provided with the distribution.
//    * Neither the name of Sergey Slyadnev nor the
//      names of all contributors may be used to endorse or promote products
//      derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------

#ifndef cascade_Polygon_HeaderFile
#define cascade_Polygon_HeaderFile

// Cascade includes
#include <mobius/cascade.h>

// Core includes
#include <mobius/core_Polygon.h>
#include <mobius/core_Ptr.h>

// OpenCascade includes
#include <BRepBuilderAPI_MakePolygon.hxx>
#include <gp_Pln.hxx>
#include <TopoDS_Wire.hxx>

namespace mobius {

//! \ingroup MOBIUS_CASCADE
//!
//! Bridge for conversions between Mobius and OCCT polygons.
class cascade_Polygon
{
public:

  static TopoDS_Wire Convert(const t_ptr<t_polygon>& mobiusPgon)
  {
    // Convert outer polygon.
    cascade_Polygon converter(mobiusPgon);
    //
    converter.DirectConvert();
    //
    return converter.GetOpenCascadeWire();
  }

public:

  //! Ctor.
  cascade_Polygon(const t_ptr<t_polygon>& mobiusPgon)
  {
    m_mobiusPgon = mobiusPgon;
    m_bIsDone    = false;
  }

  //! Dtor.
  ~cascade_Polygon() = default;

public:

  void DirectConvert()
  {
    if ( !m_mobiusPgon.IsNull() )
      this->convertToOpenCascade();
  }

public:

  const t_ptr<t_polygon>& GetMobiusPgon() const
  {
    return m_mobiusPgon;
  }

  const TopoDS_Wire& GetOpenCascadeWire() const
  {
    return m_occtPgon;
  }

  bool IsDone() const
  {
    return m_bIsDone;
  }

protected:

  void convertToOpenCascade()
  {
    // Choose working plane
    Handle(Geom_Plane) workPlane = new Geom_Plane( gp_Pln( gp::Origin(), gp::DZ() ) );

    // Get poles to convert.
    const std::vector<t_uv>& poles = m_mobiusPgon->GetPoles();

    // Populate wire builder.
    BRepBuilderAPI_MakePolygon mkPoly;
    for ( int k = 0; k < (int) ( poles.size() ); ++k )
    {
      mkPoly.Add( workPlane->Value( poles[k].U(), poles[k].V() ) );
    }
    //
    mkPoly.Add( workPlane->Value( poles[0].U(), poles[0].V() ) );

    // Build wire
    mkPoly.Build();
    m_occtPgon = mkPoly.Wire();

    // Set done.
    m_bIsDone = true;
  }

private:

  //! Mobius data structure.
  t_ptr<t_polygon> m_mobiusPgon;

  //! OCCT data structure.
  TopoDS_Wire m_occtPgon;

  //! Indicates whether conversion is done or not.
  bool m_bIsDone;

};

}

#endif
