//-----------------------------------------------------------------------------
// Created on: 08 February 2025
//-----------------------------------------------------------------------------
// Copyright (c) 2025-present, Sergey Slyadnev
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
//    * Redistributions of source code must retain the above copyright
//      notice, this list of conditions and the following disclaimer.
//    * Redistributions in binary form must reproduce the above copyright
//      notice, this list of conditions and the following disclaimer in the
//      documentation and/or other materials provided with the distribution.
//    * Neither the name of Sergey Slyadnev nor the
//      names of all contributors may be used to endorse or promote products
//      derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------

#ifndef cascade_PolygonPart_HeaderFile
#define cascade_PolygonPart_HeaderFile

// Cascade includes
#include <mobius/cascade_Polygon.h>

// Core includes
#include <mobius/core_PolygonPart.h>

// OpenCascade includes
#include <BRep_Builder.hxx>
#include <BRepBuilderAPI_MakeFace.hxx>
#include <ShapeFix_Face.hxx>
#include <TopoDS_Face.hxx>

namespace mobius {

//! \ingroup MOBIUS_CASCADE
//!
//! Bridge for conversions between Mobius' polygonal parts and OCCT shapes.
class cascade_PolygonPart
{
public:

  //! Ctor.
  cascade_PolygonPart(const t_ptr<t_polygonPart>& mobiusPart)
  {
    m_mobiusPart = mobiusPart;
    m_bIsDone    = false;
  }

  //! Dtor.
  ~cascade_PolygonPart() = default;

public:

  void DirectConvert()
  {
    if ( !m_mobiusPart.IsNull() )
      this->convertToOpenCascade();
  }

public:

  const t_ptr<t_polygonPart>& GetMobiusPart() const
  {
    return m_mobiusPart;
  }

  const TopoDS_Face& GetOpenCascadeFace() const
  {
    return m_occtPart;
  }

  bool IsDone() const
  {
    return m_bIsDone;
  }

protected:

  void convertToOpenCascade()
  {
    BRepBuilderAPI_MakeFace mkFace(cascade_Polygon::Convert( m_mobiusPart->GetOuterLoop() ), true);

    if ( mkFace.IsDone() )
    {
      TopoDS_Face F = mkFace.Face();

      const std::vector< t_ptr<t_polygon> >& holes = m_mobiusPart->GetInnerLoops();

      for ( const auto& hole : holes )
      {
        BRep_Builder().Add( F, cascade_Polygon::Convert(hole) );
      }
      ShapeFix_Face sff(F);
      sff.FixOrientation();
      m_occtPart = sff.Face();
    }

    // Set done.
    m_bIsDone = true;
  }

private:

  //! Mobius data structure.
  t_ptr<t_polygonPart> m_mobiusPart;

  //! OCCT data structure.
  TopoDS_Face m_occtPart;

  //! Indicates whether conversion is done or not.
  bool m_bIsDone;

};

}

#endif
