//-----------------------------------------------------------------------------
// Created on: 26 January 2025
//-----------------------------------------------------------------------------
// Copyright (c) 2025-present, Sergey Slyadnev
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
//    * Redistributions of source code must retain the above copyright
//      notice, this list of conditions and the following disclaimer.
//    * Redistributions in binary form must reproduce the above copyright
//      notice, this list of conditions and the following disclaimer in the
//      documentation and/or other materials provided with the distribution.
//    * Neither the name of Sergey Slyadnev nor the
//      names of all contributors may be used to endorse or promote products
//      derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------

// Own include
#include <mobius/core_BBox2d.h>

using namespace mobius;

//-----------------------------------------------------------------------------

void core_BBox2d::Get(double& umin, double& vmin,
                      double& umax, double& vmax) const
{
  umin = minPt.U();
  vmin = minPt.V();
  umax = maxPt.U();
  vmax = maxPt.V();
}

//-----------------------------------------------------------------------------

void core_BBox2d::Add(const core_UV& P)
{
  if ( IsVoid )
  {
    minPt  = P;
    maxPt  = P;
    IsVoid = false;
  }
  else
  {
    if ( P.U() < minPt.U() ) minPt.SetU( P.U() );
    if ( P.V() < minPt.V() ) minPt.SetV( P.V() );
    if ( P.U() > maxPt.U() ) maxPt.SetU( P.U() );
    if ( P.V() > maxPt.V() ) maxPt.SetV( P.V() );
  }
}

//-----------------------------------------------------------------------------

void core_BBox2d::Add(const double u, const double v)
{
  this->Add( core_UV(u, v) );
}

//-----------------------------------------------------------------------------

bool core_BBox2d::IsOut(const core_UV& P,
                        const double   tolerance) const
{
  double Umin, Vmin, Umax, Vmax;
  this->Get(Umin, Vmin, Umax, Vmax);

  if ( P.U() < Umin && std::fabs(P.U() - Umin) > tolerance ) return true;
  if ( P.U() > Umax && std::fabs(P.U() - Umax) > tolerance ) return true;

  if ( P.V() < Vmin && std::fabs(P.V() - Vmin) > tolerance ) return true;
  if ( P.V() > Vmax && std::fabs(P.V() - Vmax) > tolerance ) return true;

  return false;
}

//-----------------------------------------------------------------------------

bool core_BBox2d::IsOut(const core_BBox2d& other,
                        const double       tolerance) const
{
  double OUmin, OVmin, OUmax, OVmax;
  other.Get(OUmin, OVmin, OUmax, OVmax);

  double Umin, Vmin, Umax, Vmax;
  this->Get(Umin, Vmin, Umax, Vmax);

  if ( OUmax < Umin && std::fabs(OUmax - Umin) > tolerance ) return true;
  if ( OUmin > Umax && std::fabs(OUmin - Umax) > tolerance ) return true;

  if ( OVmax < Vmin && std::fabs(OVmax - Vmin) > tolerance ) return true;
  if ( OVmin > Vmax && std::fabs(OVmin - Vmax) > tolerance ) return true;

  return false;
}
