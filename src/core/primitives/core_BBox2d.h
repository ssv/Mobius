//-----------------------------------------------------------------------------
// Created on: 26 January 2025
//-----------------------------------------------------------------------------
// Copyright (c) 2025-present, Sergey Slyadnev
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
//    * Redistributions of source code must retain the above copyright
//      notice, this list of conditions and the following disclaimer.
//    * Redistributions in binary form must reproduce the above copyright
//      notice, this list of conditions and the following disclaimer in the
//      documentation and/or other materials provided with the distribution.
//    * Neither the name of Sergey Slyadnev nor the
//      names of all contributors may be used to endorse or promote products
//      derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------

#ifndef core_BBox2d_HeaderFile
#define core_BBox2d_HeaderFile

// Core includes
#include <mobius/core_UV.h>

namespace mobius {

//! \ingroup MOBIUS_CORE
//!
//! Axis-aligned bounding box in 2D.
struct core_BBox2d
{
  bool IsVoid; //!< Am I empty?
  t_uv minPt;  //!< Min corner point.
  t_uv maxPt;  //!< Max corner point.

  core_BBox2d() : IsVoid(true) {} //!< Default ctor.

  core_BBox2d(const double umin,
              const double vmin,
              const double umax,
              const double vmax)
  //
  : IsVoid(false)
  {
    minPt = t_uv(umin, vmin);
    maxPt = t_uv(umax, vmax);
  }

  mobiusCore_EXPORT void
    Get(double& umin, double& vmin,
        double& umax, double& vmax) const;

  mobiusCore_EXPORT void
    Add(const t_uv& P);

  mobiusCore_EXPORT void
    Add(const double u, const double v);

  mobiusCore_EXPORT bool
    IsOut(const t_uv&  P,
          const double tolerance) const;

  mobiusCore_EXPORT bool
    IsOut(const core_BBox2d& other,
          const double       tolerance) const;

};

}

#endif
