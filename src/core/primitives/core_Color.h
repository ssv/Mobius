//-----------------------------------------------------------------------------
// Created on: July 2018
//-----------------------------------------------------------------------------
// Copyright (c) 2018-present, Sergey Slyadnev
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
//    * Redistributions of source code must retain the above copyright
//      notice, this list of conditions and the following disclaimer.
//    * Redistributions in binary form must reproduce the above copyright
//      notice, this list of conditions and the following disclaimer in the
//      documentation and/or other materials provided with the distribution.
//    * Neither the name of Sergey Slyadnev nor the
//      names of all contributors may be used to endorse or promote products
//      derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------

#ifndef core_Color_HeaderFile
#define core_Color_HeaderFile

// Core includes
#include <mobius/core.h>

namespace mobius {

//-----------------------------------------------------------------------------

//! \ingroup MOBIUS_CORE
//!
//! Simplistic structure to hold color components.
struct core_Color
{
  double fRed;   //!< Red component in range [0, 1].
  double fGreen; //!< Green component in range [0, 1].
  double fBlue;  //!< Blue component in range [0, 1].

  core_Color() : fRed(0.), fGreen(0.), fBlue(0.) {} //!< Default ctor.

  //! Complete ctor.
  //! \param[in] _r red component to set.
  //! \param[in] _g green component to set.
  //! \param[in] _b blue component to set.
  core_Color(const double _r, const double _g, const double _b)
  : fRed(_r), fGreen(_g), fBlue(_b) {}
};

}

#define MobiusColor_Red     mobius::core_Color(1.0, 000, 000)
#define MobiusColor_Green   mobius::core_Color(000, 1.0, 000)
#define MobiusColor_Blue    mobius::core_Color(000, 000, 1.0)
#define MobiusColor_Yellow  mobius::core_Color(1.0, 1.0, 000)
#define MobiusColor_White   mobius::core_Color(1.0, 1.0, 1.0)
#define MobiusColor_Black   mobius::core_Color(000, 000, 000)
#define MobiusColor_Magenta mobius::core_Color(1.0, 000, 1.0)

#endif
