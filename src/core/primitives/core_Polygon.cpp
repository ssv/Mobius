//-----------------------------------------------------------------------------
// Created on: 15 May 2024
//-----------------------------------------------------------------------------
// Copyright (c) 2024, Sergey Slyadnev
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
//    * Redistributions of source code must retain the above copyright
//      notice, this list of conditions and the following disclaimer.
//    * Redistributions in binary form must reproduce the above copyright
//      notice, this list of conditions and the following disclaimer in the
//      documentation and/or other materials provided with the distribution.
//    * Neither the name of Sergey Slyadnev nor the
//      names of all contributors may be used to endorse or promote products
//      derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------

// Own include
#include <mobius/core_PointInPoly.h>
#include <mobius/core_Polygon.h>

// Geometry includes
#include <mobius/Intf_InterferencePolygon2d.hxx>
#include <mobius/Intf_Polygon2d.hxx>

// Core includes
#include <mobius/core_JSON.h>

// Standard includes
#include <cfloat>

using namespace mobius;

#define TOT_VERTS 10000

static double pgon[TOT_VERTS][2];

//-----------------------------------------------------------------------------

namespace
{
  //! Polygon class for OpenCascade's intersection algorithm.
  class IntfPolygon : public occ::Intf_Polygon2d
  {
  public:

    //! Ctor to convert the native polygon of Mobius to the one
    //! of OpenCascade.
    IntfPolygon(const t_ptr<core_Polygon>& polygon)
    {
      this->Init(polygon);
    }

    //! Ctor with the initializer list.
    IntfPolygon(const std::initializer_list<std::pair<double, double>>& poles)
    {
      for ( const auto& P : poles )
      {
        occ::gp_Pnt2d P2d(P.first, P.second);
        //
        m_poles.push_back( occ::gp_Pnt2d(P.first, P.second) );
        myBox.Add(P2d);
      }
    }

  public:

    //! Initializes this polygon from the native data structure
    //! of Mobius.
    void Init(const t_ptr<core_Polygon>& polygon)
    {
      const std::vector<t_uv>& poles = polygon->GetPoles();
      //
      for ( const auto& uv : poles )
      {
        occ::gp_Pnt2d P2d( uv.U(), uv.V() );
        //
        m_poles.push_back(P2d);
        myBox.Add(P2d);
      }
    }

    //! Returns the tolerance of the polygon.
    virtual double DeflectionOverEstimation() const
    {
      return core_Precision::Resolution3D();
    }

    //! Returns the number of segments in the polygon.
    virtual int NbSegments() const
    {
      return int( m_poles.size() - 1 );
    }

    //! Returns the points of the segment <index> in the Polygon.
    virtual void Segment(const int      index,
                         occ::gp_Pnt2d& beg,
                         occ::gp_Pnt2d& end) const
    {
      beg = m_poles[index - 1];
      end = m_poles[index];
    }

    //! \return true if the polygon is closed, false -- otherwise.
    virtual bool Closed() const
    {
      return true; // We assume that nestable polygons are all closed.
    }

  protected:
 
    std::vector<occ::gp_Pnt2d> m_poles;

  };
}

//-----------------------------------------------------------------------------

t_ptr<core_Polygon>
  core_Polygon::Import(const std::string& filename)
{
  std::ifstream FILE(filename);

  if ( !FILE.is_open() )
    return nullptr;

  t_ptr<core_Polygon> res = new core_Polygon;

  while ( !FILE.eof() )
  {
    char str[256];
    FILE.getline(str, 256);

    std::vector<std::string> tokens;
    std::istringstream iss(str);
    std::copy( std::istream_iterator<std::string>(iss),
               std::istream_iterator<std::string>(),
               std::back_inserter< std::vector<std::string> >(tokens) );

    if ( tokens.empty() || tokens.size() < 2 )
      continue;

    double x = atof( tokens[0].c_str() );
    double y = atof( tokens[1].c_str() );

    res->AddPole( t_uv(x, y) );
  }

  FILE.close();
  return res;
}

//-----------------------------------------------------------------------------

core_Polygon::core_Polygon()
//
: core_OBJECT()
{}

//-----------------------------------------------------------------------------

core_Polygon::core_Polygon(const std::vector<t_uv>& poles)
//
: core_OBJECT (),
  m_poles     (poles)
{}

//-----------------------------------------------------------------------------

core_Polygon::core_Polygon(const std::initializer_list< std::pair<double, double> >& poles)
//
: core_OBJECT()
{
  for ( const auto& P : poles )
  {
    this->AddPole( t_uv(P.first, P.second) );
  }
}

//-----------------------------------------------------------------------------

core_Polygon::~core_Polygon()
{
}

//-----------------------------------------------------------------------------

void core_Polygon::Clear()
{
  m_poles.clear();
}

//-----------------------------------------------------------------------------

void core_Polygon::AddPole(const t_uv& P)
{
  m_poles.push_back(P);
}

//-----------------------------------------------------------------------------

bool core_Polygon::Close(const double tol)
{
  const t_uv& front = m_poles.front();
  const t_uv& back  = m_poles.back();

  if ( front.IsEqual(back, tol) )
    return false;

  this->AddPole(front);
  return true;
}

//-----------------------------------------------------------------------------

t_ptr<core_Polygon> core_Polygon::Copy() const
{
  return new core_Polygon(m_poles);
}

//-----------------------------------------------------------------------------

void core_Polygon::GetBounds(double& uMin, double& uMax,
                             double& vMin, double& vMax) const
{
  double u_min = DBL_MAX, u_max = -DBL_MAX;
  double v_min = DBL_MAX, v_max = -DBL_MAX;

  // Iterate over the points to calculate bounds.
  for ( int p = 0; p < (int) m_poles.size(); ++p )
  {
    const t_uv& P = m_poles.at(p);
    const double u = P.U(),
                 v = P.V();

    if ( u > u_max )
      u_max = u;
    if ( u < u_min )
      u_min = u;
    if ( v > v_max )
      v_max = v;
    if ( v < v_min )
      v_min = v;
  }

  // Set results.
  uMin = u_min;
  uMax = u_max;
  vMin = v_min;
  vMax = v_max;
}

//-----------------------------------------------------------------------------

void core_Polygon::Move(const t_uv& T)
{
  for ( auto& p : m_poles )
  {
    p += T;
  }
}

//-----------------------------------------------------------------------------

void core_Polygon::Move(const core_IsoTransform& T)
{
  for ( auto& p : m_poles )
  {
    t_xyz p3d = T.Apply( t_xyz( p.U(), p.V(), 0.) );
    //
    p.SetU( p3d.X() );
    p.SetV( p3d.Y() );
  }
}

//-----------------------------------------------------------------------------

void core_Polygon::Move(const core_IsoTransformChain& T)
{
  for ( auto& p : m_poles )
  {
    t_xyz p3d = T.Apply( t_xyz( p.U(), p.V(), 0.) );
    //
    p.SetU( p3d.X() );
    p.SetV( p3d.Y() );
  }
}

//-----------------------------------------------------------------------------

bool core_Polygon::Intersects(const t_ptr<core_Polygon>& other,
                              std::vector<t_uv>&         ipts) const
{
  // Prepare operand adaptors.
  IntfPolygon poly[2] = { IntfPolygon(this), IntfPolygon(other) };

  // Compute intersections.
  occ::Intf_InterferencePolygon2d algo(poly[0], poly[1]);

  // Check the intersection result.
  const int numPts = algo.NbSectionPoints();
  //
  for ( int isol = 1; isol <= numPts; ++isol )
  {
    occ::gp_Pnt2d sol = algo.Pnt2dValue(isol);
    //
    ipts.push_back( t_uv( sol.X(), sol.Y() ) );
  }

  return !ipts.empty();
}

//-----------------------------------------------------------------------------

bool core_Polygon::Intersects(const t_ptr<core_Polygon>& other) const
{
  std::vector<t_uv> ipts;

  return this->Intersects(other, ipts);
}

//-----------------------------------------------------------------------------

bool core_Polygon::IsPointInside(const t_uv& pt) const
{
  // Populate the array of poles.
  int idx = 0;
  const std::vector<t_uv>& poles = this->GetPoles();
  //
  for ( const auto& uv : poles )
  {
    pgon[idx][0] = uv.U();
    pgon[idx][1] = uv.V();
    idx++;
  }

  // Populate the array of point coordinates.
  double point[2] = { pt.U(), pt.V() };

  // Ray-based PMC test.
  const int state = CrossingsTest(pgon, int( poles.size() ), point);

  return (state == 1);
}

//-----------------------------------------------------------------------------

bool core_Polygon::IsInsideOf(const t_ptr<core_Polygon>& other) const
{
  const std::vector<t_uv>& poles = this->GetPoles();
  //
  for ( const auto& uv : poles )
  {
    if ( !other->IsPointInside(uv) )
      return false;
  }

  return true; // All poles of this polygon are inside of the other one.
}

//-----------------------------------------------------------------------------

t_uv core_Polygon::ComputeCenter() const
{
  t_uv cog;

  const std::vector<t_uv>& poles = this->GetPoles();
  //
  for ( const auto& uv : poles )
  {
    cog += uv;
  }
  //
  cog /= (int) poles.size();

  return cog;
}

//-----------------------------------------------------------------------------

const std::vector<t_uv>& core_Polygon::GetPoles() const
{
  return m_poles;
}

//-----------------------------------------------------------------------------

std::string core_Polygon::SerializePoles() const
{
  return core_JSON::FromCollection(m_poles);
}
