//-----------------------------------------------------------------------------
// Created on: 15 May 2024
//-----------------------------------------------------------------------------
// Copyright (c) 2024, Sergey Slyadnev
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
//    * Redistributions of source code must retain the above copyright
//      notice, this list of conditions and the following disclaimer.
//    * Redistributions in binary form must reproduce the above copyright
//      notice, this list of conditions and the following disclaimer in the
//      documentation and/or other materials provided with the distribution.
//    * Neither the name of Sergey Slyadnev nor the
//      names of all contributors may be used to endorse or promote products
//      derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------

#ifndef nest_Polygon_HeaderFile
#define nest_Polygon_HeaderFile

// Nesting includes
#include <mobius/nest.h>

// Core includes
#include <mobius/core_IsoTransformChain.h>
#include <mobius/core_OBJECT.h>
#include <mobius/core_Precision.h>
#include <mobius/core_Ptr.h>
#include <mobius/core_UV.h>

namespace mobius {

//! \ingroup MOBIUS_CORE
//!
//! 2D polygon for nesting.
class core_Polygon : public core_OBJECT
{
public:

  //! Reads polygon poles from XY-format file.
  //! \param[in] filename the input filename.
  //! \return the constructed polygon.
  mobiusCore_EXPORT static t_ptr<core_Polygon>
    Import(const std::string& filename);

// Construction & destruction:
public:

  //! Ctor.
  mobiusCore_EXPORT
    core_Polygon();

  //! Ctor with the vector of poles.
  //! \param[in] poles the poles to initialize the polygon.
  mobiusCore_EXPORT
    core_Polygon(const std::vector<t_uv>& poles);

  //! Ctor with initializer list.
  //! \param[in] poles the poles to initialize the polygon.
  mobiusCore_EXPORT
    core_Polygon(const std::initializer_list< std::pair<double, double> >& poles);

  //! Dtor.
  mobiusCore_EXPORT virtual
    ~core_Polygon();

public:

  /** @name Initializers
   *  Methods to initialize the polygon.
   */
  //@{

  //! Erases all poles in the polygon.
  mobiusCore_EXPORT void
    Clear();

  //! Adds the passed two-dimensional point as another pole of the polygon.
  //! \param[in] P the point to add as a pole.
  mobiusCore_EXPORT void
    AddPole(const t_uv& P);

  //! Adds the first point also as the last point of the polygon in order to
  //! forcibly close it. This function verifies whether a polygon is already
  //! closed and takes no action if the last point is equal to the initial
  //! point with the specified tolerance.
  //!
  //! \param[in] tol the coincidence tolerance to use to check whether the
  //!                last pole is the same as the first pole.
  //!
  //! \return false if no pole has been added, false -- otherwise (the polygon
  //!         has been closed).
  mobiusCore_EXPORT bool
    Close(const double tol = core_Precision::Resolution3D());

  //! \return deep copy of this polygon.
  mobiusCore_EXPORT t_ptr<core_Polygon>
    Copy() const;

  //@}

public:

  /** @name Computations
   *  Geometric computations.
   */
  //@{

  //! Computes the two-dimensional bounds of the polygon.
  mobiusCore_EXPORT void
    GetBounds(double& uMin, double& uMax,
              double& vMin, double& vMax) const;

  //! Moves each pole by the passed vector `T`.
  mobiusCore_EXPORT void
    Move(const t_uv& T);

  //! Applies transformation `T` to each pole of the polygon.
  mobiusCore_EXPORT void
    Move(const core_IsoTransform& T);

  //! Applies transformation chain `T` to each pole of the polygon.
  mobiusCore_EXPORT void
    Move(const core_IsoTransformChain& T);

  //! Checks if this polygon intersects the passed `other` one. This
  //! method returns `true` only if the polygons' segments cross each
  //! other. If, for example, one polygon is contained within another,
  //! then there are no intersection points and `false` is returned
  //! as a result.
  //!
  //! \param[in]  other the other polygon to intersect this one with.
  //! \param[out] ipts  the computed intersection points (if any).
  //! \return true if two polygons are intersecting.
  mobiusCore_EXPORT bool
    Intersects(const t_ptr<core_Polygon>& other,
               std::vector<t_uv>&         ipts) const;

  //! Checks if this polygon intersects the passed `other` one. This
  //! method returns `true` only if the polygons' segments cross each
  //! other. If, for example, one polygon is contained within another,
  //! then there are no intersection points and `false` is returned
  //! as a result.
  //!
  //! \param[in] other the other polygon to intersect this one with.
  //! \return true if two polygons are intersecting.
  mobiusCore_EXPORT bool
    Intersects(const t_ptr<core_Polygon>& other) const;

  //! Checks if the passed point is inside this polygon or not.
  //! \param[in] pt the point to test.
  //! \return true if the passed `pt` point falls inside the polygon's
  //!         interior, false -- otherwise.
  mobiusCore_EXPORT bool
    IsPointInside(const t_uv& pt) const;

  //! Checks if this polygon is located inside the passed `other` polygon,
  //! i.e., it fully resides in the interior of the `other` one.
  //! \param[in] other the other polygon to check if it contains this one.
  //! \return true if the `other` polygon contains this one in its interior,
  //!         false -- otherwise.
  mobiusCore_EXPORT bool
    IsInsideOf(const t_ptr<core_Polygon>& other) const;

  //! \return the center point (COG) of the polygon.
  mobiusCore_EXPORT t_uv
    ComputeCenter() const;

  //@}

public:

  //! \return const reference to the stored poles.
  mobiusCore_EXPORT const std::vector<t_uv>&
    GetPoles() const;

  //! \return poles as a string array.
  mobiusCore_EXPORT std::string
    SerializePoles() const;

protected:

  std::vector<t_uv> m_poles; //!< Polygon poles.

};

//! \ingroup MOBIUS_CORE
//!
//! Convenience alias.
typedef core_Polygon t_polygon;

}

#endif
