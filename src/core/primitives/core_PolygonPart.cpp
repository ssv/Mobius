//-----------------------------------------------------------------------------
// Created on: 28 January 2025
//-----------------------------------------------------------------------------
// Copyright (c) 2025-present, Sergey Slyadnev
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
//    * Redistributions of source code must retain the above copyright
//      notice, this list of conditions and the following disclaimer.
//    * Redistributions in binary form must reproduce the above copyright
//      notice, this list of conditions and the following disclaimer in the
//      documentation and/or other materials provided with the distribution.
//    * Neither the name of Sergey Slyadnev nor the
//      names of all contributors may be used to endorse or promote products
//      derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------

// Own include
#include <mobius/core_PolygonPart.h>

// core includes
#include <mobius/core_BBox2d.h>

// Standard includes
#include <algorithm>

using namespace mobius;

//-----------------------------------------------------------------------------

core_PolygonPart::core_PolygonPart()
//
: core_OBJECT()
{}

//-----------------------------------------------------------------------------

core_PolygonPart::core_PolygonPart(const t_ptr<t_polygon>&                outer,
                                   const std::vector< t_ptr<t_polygon> >& inner)
//
: core_OBJECT(),
  m_outer    (outer),
  m_holes    (inner)
{}

//-----------------------------------------------------------------------------

core_PolygonPart::~core_PolygonPart()
{}

//-----------------------------------------------------------------------------

void core_PolygonPart::Clear()
{
  // Clear loops.
  m_outer->Clear();
  //
  for ( auto& il : m_holes )
  {
    il->Clear();
  }
}

//-----------------------------------------------------------------------------

void core_PolygonPart::GetBounds(double& uMin, double& uMax,
                                 double& vMin, double& vMax) const
{
  m_outer->GetBounds(uMin, uMax, vMin, vMax);
}

//-----------------------------------------------------------------------------

void core_PolygonPart::Move(const t_uv& T)
{
  m_outer->Move(T);

  for ( auto& il : m_holes )
  {
    il->Move(T);
  }
}

//-----------------------------------------------------------------------------

void core_PolygonPart::Move(const core_IsoTransform& T)
{
  m_outer->Move(T);

  for ( auto& il : m_holes )
  {
    il->Move(T);
  }
}

//-----------------------------------------------------------------------------

void core_PolygonPart::Move(const core_IsoTransformChain& T)
{
  m_outer->Move(T);

  for ( auto& il : m_holes )
  {
    il->Move(T);
  }
}

//-----------------------------------------------------------------------------

bool core_PolygonPart::IsPointInside(const t_uv& pt) const
{
  bool outerOk = m_outer->IsPointInside(pt);
  //
  if ( !outerOk )
    return false;

  bool innerOk = true;
  //
  for ( const auto& il : m_holes )
  {
    if ( il->IsPointInside(pt) )
    {
      innerOk = false;
      break;
    }
  }

  return innerOk;
}

//-----------------------------------------------------------------------------

bool core_PolygonPart::Collides(const t_ptr<core_PolygonPart>& other,
                                std::vector<t_uv>&             ipts) const
{
  double uMin[2], uMax[2], vMin[2], vMax[2];

  this ->GetBounds(uMin[0], uMax[0], vMin[0], vMax[0]);
  other->GetBounds(uMin[1], uMax[1], vMin[1], vMax[1]);

  core_BBox2d bbox2d[2] = {
    core_BBox2d(uMin[0], vMin[0], uMax[0], vMax[0]),
    core_BBox2d(uMin[1], vMin[1], uMax[1], vMax[1])
  };

  if ( bbox2d[1].IsOut( bbox2d[0], core_Precision::Resolution3D() ) )
    return false;

  // O-O (outer-outer)
  if ( this->GetOuterLoop()->Intersects( other->GetOuterLoop(), ipts ) )
    return true;

  // O-I (outer-inner)
  const std::vector< t_ptr<t_polygon> >& otherInner = other->GetInnerLoops();
  //
  for ( const auto& il : otherInner )
  {
    if ( this->GetOuterLoop()->Intersects(il, ipts) )
      return true;
  }

  // I-O (inner-outer)
  const std::vector< t_ptr<t_polygon> >& thisInner = this->GetInnerLoops();
  //
  for ( const auto& il : thisInner )
  {
    if ( il->Intersects( other->GetOuterLoop(), ipts ) )
      return true;
  }

  // I-I (inner-inner)
  for ( const auto& til : thisInner )
  {
    for ( const auto& oil : otherInner )
    {
      if ( til->Intersects(oil, ipts) )
        return true;
    }
  }

  // TODO: check full enclosure

  return false;
}

//-----------------------------------------------------------------------------

t_uv core_PolygonPart::ComputeCenter() const
{
  return this->GetOuterLoop()->ComputeCenter();
}
