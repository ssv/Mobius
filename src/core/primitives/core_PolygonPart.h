//-----------------------------------------------------------------------------
// Created on: 28 January 2025
//-----------------------------------------------------------------------------
// Copyright (c) 2025-present, Sergey Slyadnev
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
//    * Redistributions of source code must retain the above copyright
//      notice, this list of conditions and the following disclaimer.
//    * Redistributions in binary form must reproduce the above copyright
//      notice, this list of conditions and the following disclaimer in the
//      documentation and/or other materials provided with the distribution.
//    * Neither the name of Sergey Slyadnev nor the
//      names of all contributors may be used to endorse or promote products
//      derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------

#ifndef core_PolygonPart_HeaderFile
#define core_PolygonPart_HeaderFile

// Core includes
#include <mobius/core_Polygon.h>

namespace mobius {

//! \ingroup MOBIUS_CORE
//!
//! 2D part composed of a series of polygons, one being the outer contour
//! and optional child contours for holes.
class core_PolygonPart : public core_OBJECT
{
// Construction & destruction:
public:

  //! Ctor.
  mobiusCore_EXPORT
    core_PolygonPart();

  //! Ctor with the contour polygons.
  //! \param[in] outer the outer polygon.
  //! \param[in] inner the inner polygons to represent holes and cutouts.
  mobiusCore_EXPORT
    core_PolygonPart(const t_ptr<t_polygon>&                outer,
                     const std::vector< t_ptr<t_polygon> >& inner = std::vector< t_ptr<t_polygon> >());

  //! Dtor.
  mobiusCore_EXPORT virtual
    ~core_PolygonPart();

public:

  //! Sets the outer loop for this part.
  void SetOuterLoop(const t_ptr<t_polygon>& ol)
  {
    m_outer = ol;
  }

  //! \return the polygon of the outer loop.
  const t_ptr<t_polygon>& GetOuterLoop() const
  {
    return m_outer;
  }

  //! \return non-const reference to the outer loop.
  t_ptr<t_polygon>& ChangeOuterLoop()
  {
    return m_outer;
  }

  //! Adds another polygon to the collection of inner loops.
  void AddInnerLoop(const t_ptr<t_polygon>& polygon)
  {
    m_holes.push_back(polygon);
  }

  //! \return the collection of the inner-loop polygons.
  const std::vector< t_ptr<t_polygon> >& GetInnerLoops() const
  {
    return m_holes;
  }

  //! \return non-const reference to the inner loops.
  std::vector< t_ptr<t_polygon> >& ChangeInnerLoop()
  {
    return m_holes;
  }

public:

  //! Erases all poles in the part.
  mobiusCore_EXPORT virtual void
    Clear();

public:

  //! Prepares a copy of this part.
  virtual t_ptr<core_PolygonPart> Copy() const
  {
    t_ptr<core_PolygonPart> copy = new core_PolygonPart;

    copy->m_outer = m_outer;
    copy->m_holes = m_holes;

    return copy;
  }

public:

  //! Computes the two-dimensional bounds of the part.
  mobiusCore_EXPORT void
    GetBounds(double& uMin, double& uMax,
              double& vMin, double& vMax) const;

  //! Moves each pole by the passed vector `T`.
  mobiusCore_EXPORT void
    Move(const t_uv& T);

  //! Applies transformation `T` to each pole of the part.
  mobiusCore_EXPORT void
    Move(const core_IsoTransform& T);

  //! Applies transformation chain `T` to each pole of the part.
  mobiusCore_EXPORT void
    Move(const core_IsoTransformChain& T);

  //! Checks if the passed point is inside this part or not.
  //! \param[in] pt the point to test.
  //! \return true if the passed `pt` point falls inside the part's
  //!         interior, false -- otherwise.
  mobiusCore_EXPORT bool
    IsPointInside(const t_uv& pt) const;

  //! Checks if this part collides with the other one.
  //! \param[in]  other the part to collide with.
  //! \param[out] ipts  the collected intersection points.
  //! \return true in the case of collision is detected, false -- otherwise.
  mobiusCore_EXPORT bool
    Collides(const t_ptr<core_PolygonPart>& other,
             std::vector<t_uv>&             ipts) const;

  //! \return the center point (COG) of the part.
  mobiusCore_EXPORT t_uv
    ComputeCenter() const;

protected:

  t_ptr<t_polygon>                m_outer; //!< Outer contour.
  std::vector< t_ptr<t_polygon> > m_holes; //!< Inner contours.

};

//! \ingroup MOBIUS_CORE
//!
//! Convenience alias.
typedef core_PolygonPart t_polygonPart;

}

#endif
