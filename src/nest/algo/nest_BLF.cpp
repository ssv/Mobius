//-----------------------------------------------------------------------------
// Created on: 27 January 2025
//-----------------------------------------------------------------------------
// Copyright (c) 2025-present, Sergey Slyadnev
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
//    * Redistributions of source code must retain the above copyright
//      notice, this list of conditions and the following disclaimer.
//    * Redistributions in binary form must reproduce the above copyright
//      notice, this list of conditions and the following disclaimer in the
//      documentation and/or other materials provided with the distribution.
//    * Neither the name of Sergey Slyadnev nor the
//      names of all contributors may be used to endorse or promote products
//      derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------

// Own include
#include <mobius/nest_BLF.h>

// Core includes
#include <mobius/core_Timer.h>

// Standard includes
#include <algorithm>

using namespace mobius;

#define ANGLE_CONFUSION_DEG 5.

//-----------------------------------------------------------------------------

namespace
{
  void CleanUpDiagnostics(const std::vector< t_ptr<nest_Part> >& variants,
                          geom_PlotterEntry                      plotter)
  {
    /*for ( const auto& V : variants )
      plotter.ERASE( V->GetName() );

    plotter.ERASE( "cursor" );*/
  }
}

//-----------------------------------------------------------------------------

nest_BLF::nest_BLF(core_ProgressEntry progress,
                   geom_PlotterEntry  plotter)
//
: geom_IAlgorithm (progress, plotter)
{}

//-----------------------------------------------------------------------------

nest_BLF::nest_BLF(const t_ptr<nest_Sheet>& container,
                   core_ProgressEntry       progress,
                   geom_PlotterEntry        plotter)
//
: geom_IAlgorithm (progress, plotter)
{
  this->Init(container);
}

//-----------------------------------------------------------------------------

void nest_BLF::Init(const t_ptr<nest_Sheet>& container)
{
  m_container = container;
}

//-----------------------------------------------------------------------------

bool nest_BLF::PlacePart(const t_ptr<nest_Part>& prototype,
                         t_ptr<nest_Part>&       nestedVariant,
                         double&                 elapsedTime,
                         int&                    numColTests)
{
  MOBIUS_TIMER_NEW
  MOBIUS_TIMER_GO

  numColTests = 0;

  /* ==============================
   *  Prepare variants for nesting.
   * ============================== */

  // Create rotated copies of the part to nest.
  std::vector< t_ptr<nest_Part> > variants;
  //
  this->prepareVariants(prototype, variants);
  this->preparePatterns(variants);

  //::CleanUpDiagnostics(variants, m_plotter);

  // Check if this part has been nested before, so we can reuse its
  // last nested position as a starting cursor's position.
  if ( !prototype->GetLastNestedPos().IsInitialized() )
  {
    prototype->SetLastNestedPos({0, 0});
  }

  /* ================================
   *  Loop over the nesting variants.
   * ================================ */

  // Unlock all points in the grid.
  m_container->UnlockAll();

  // Snapping grid.
  const t_ptr<SheetGrid>& grid = m_container->GetGrid();

  // Unlock all points in the grid so that after the algorithm's run we don't have
  // any points remaining locked.
  m_container->UnlockAll();

  //// Check the anchor point of the variant.
  //if ( !variant->HasAnchorPoint() )
  //{
  //  m_progress.SendLogMessage(MobiusErr(Normal) << "Anchor point is not initialized.");
  //  return false;
  //}

  // Main DBLF loop.
  bool stop   = false;
  bool nested = false;
  //
  do
  {
    std::vector< t_ptr<nest_Part> > promisingVariants, clearVariants;

    // Look-up the container for the next vacant point.
    nest_Pos G_ij;
    int      G_r = -1;
    int      P_s = grid->FromPair(prototype->GetLastNestedPos().i,
                                  prototype->GetLastNestedPos().j);
    //
    for ( int k = P_s; k < grid->M; ++k )
    {
      const nest_GridPt& P = grid->pArray[k];

      if ( !P.occupied && !P.locked )
      {
        G_r = k;
        grid->FromPlain(k, G_ij.i, G_ij.j);
        break;
      }
    }
    //
    if ( !G_ij.IsInitialized() )
    {
      stop = true;
      continue;
    }

    // Lock current snapping point.
    grid->pArray[G_r].locked = true;

    ///
    /*if ( G_r == 78 )
    {
      for ( const auto& V : variants )
      {
        m_plotter.DRAW_POLYGON_PART(V, MobiusColor_Yellow, V->GetName());
      }
    }*/
    ///

    // Check if this point has large enough neighborhood.
    nest_Nbh N;
    //
    this->getNeighborhood(G_ij, N);
    //
    N.isRoomy = this->isCursorPromising(N, variants, promisingVariants);

    // Diagnostics.
    this->drawCursor(N);

    if ( m_progress.IsCancelling() )
      return false;

    if ( !N.isRoomy )
    {
      // No luck with `G_ij`, let's move on.
      continue;
    }

    // Move promising variants to the current position point, so that
    // the variant's anchor point coincides with the grid point.
    for ( auto& variant : promisingVariants )
    {
      variant->MoveTo(G_ij);
    }

    // Check for collisions between the newly placed variants and
    // all "fixed" parts in the container. This check brings us
    // the collision-free (or "clear") variants.
    numColTests++;
    //
    if ( this->haveCollisions(promisingVariants, clearVariants) )
    {
      /*for ( const auto& V : promisingVariants )
      {
        m_plotter.DRAW_POLYGON_PART( V, MobiusColor_Red, V->GetName() );
      }*/

      prototype->SetLastNestedPos(G_ij);
      continue;
    }

    /* If here, no collisions */

    // Choose the best variant out there.
    t_ptr<nest_Part> variant = this->chooseBest(clearVariants);

    // Move part's pattern accordingly.
    variant->MovePatternTo(G_ij);
    //
    //m_plotter.REDRAW_POLYGON_PART( variant->GetName(), variant, MobiusColor_White );

    // Add the passed part to the result.
    this->addNested(variant);

    // Set the output.
    nestedVariant = variant;

    // Get all the grid points spanned by the part.
    std::vector<nest_Pos> R;
    //
    this->getSpannedPoints(variant, R);

    // Occupy all these points.
    for ( const auto& next_ijk : R )
    {
      grid->Access(next_ijk.i, next_ijk.j).occupied = true;
    }
    //
    grid->pArray[G_r].occupied = true; // Occupy current snapping point.

    // Store the last nested position.
    prototype->SetLastNestedPos(G_ij);

    // We're done, let's stop here.
    nested = stop = true;

    //::CleanUpDiagnostics(variants, m_plotter);
  }
  while ( !stop );
  //

  // Unlock all points in the grid so that after the algorithm's run we don't have
  // any points remaining locked.
  m_container->UnlockAll();

  MOBIUS_TIMER_FINISH
  elapsedTime = __aux_debug_Seconds + 60*__aux_debug_Minutes + 60*60*__aux_debug_Hours;

  return !nestedVariant.IsNull();
}

//-----------------------------------------------------------------------------

void nest_BLF::prepareVariants(const t_ptr<nest_Part>&          part,
                               std::vector< t_ptr<nest_Part> >& variants) const
{
  const double angStepDeg = std::fabs( part->GetAngleStepDeg() );
  //
  if ( angStepDeg < core_Precision::Resolution3D() )
  {
    // Make a named copy.
    t_ptr<nest_Part> copy = t_ptr<nest_Part>::DownCast( part->Copy() );
    //
    copy->SetName( part->GetName() );

    // Add a variant.
    variants.push_back(copy);
    return;
  }

  // Collect rotated variants.
  double accumAngle = 0.;
  do
  {
    // Make a named copy.
    t_ptr<nest_Part>
      copy = part->CopyRotated(accumAngle);
    //
    copy->SetName      ( part->GetName() + ":" + core::str::to_string(accumAngle) );
    copy->SetPrototype ( part->GetPrototype().IsNull() ? part : part->GetPrototype() );

    // Add a variant.
    variants.push_back(copy);

    // Increment the angle.
    accumAngle += angStepDeg;
    //
    if ( std::fabs(accumAngle - 360.) < ANGLE_CONFUSION_DEG )
      accumAngle = 360.;
  }
  while ( accumAngle < 360. );
}

//-----------------------------------------------------------------------------

void nest_BLF::preparePatterns(std::vector< t_ptr<nest_Part> >& variants) const
{
  for ( auto& variant : variants )
  {
    if ( variant->HasPattern() )
      continue;

    // Heavy operation.
    m_progress.SendLogMessage(MobiusInfo(Normal) << "Recomputing part's point pattern...");

    // Store key points of a part w.r.t. the current grid.
    m_container->BuildPointPattern(variant);
  }
}

//-----------------------------------------------------------------------------

bool nest_BLF::haveCollisions(const std::vector< t_ptr<nest_Part> >& variants,
                              std::vector< t_ptr<nest_Part> >&       result) const
{
  const std::vector< t_ptr<nest_Part> >& allNested = m_container->GetNested();

  for ( const auto& V : variants )
  {
    bool collides = false;
    for ( auto& nested : allNested )
    {
      std::vector<t_uv> ipts;
      //
      if ( nested->Collides(V, ipts) )
      {
        collides = true;
        break;
      }
    }

    if ( !collides )
      result.push_back(V);
  }

  return result.empty(); // No variants means that all variants have collisions.
}

//-----------------------------------------------------------------------------

void nest_BLF::addNested(const t_ptr<nest_Part>& part)
{
  m_container->AddNested(part);
}

//-----------------------------------------------------------------------------

void nest_BLF::getSpannedPoints(const t_ptr<nest_Part>& part,
                                std::vector<nest_Pos>&  pts) const
{
  const std::vector<nest_Pos>& pattern = part->GetPattern();

  for ( const auto& pt : pattern )
  {
    pts.push_back( {pt.i, pt.j} );
  }
}

//-----------------------------------------------------------------------------

void nest_BLF::getNeighborhood(const nest_Pos& ij,
                               nest_Nbh&       neighbors) const
{
  const t_ptr<SheetGrid>& grid = m_container->GetGrid();

  neighbors.origin    = ij;
  neighbors.container = m_container;

  double D_i = 0., D_j = 0.;
  bool stop;
  int i, j;

  // i+
  i = 1;
  stop = false;
  do
  {
    if ( ij.i != grid->Nx )
      D_i += grid->CellSize;

    if ( i >= grid->Nx - ij.i )
    {
      stop = true;
      continue;
    }

    if ( grid->Access(ij.i + i, ij.j).occupied ||
         grid->Access(ij.i + i, ij.j).locked )
    {
      stop = true;
      continue;
    }

    neighbors.pts.push_back( nest_Pos(ij.i + i, ij.j) );
    neighbors.iPos.push_back( nest_Pos(ij.i + i, ij.j) );
    ++i;
  }
  while ( !stop );

  // i-
  i = 1;
  stop = false;
  do
  {
    if ( ij.i != 0 )
      D_i += grid->CellSize;

    if ( (ij.i - i) < 0 )
    {
      stop = true;
      continue;
    }

    if ( grid->Access(ij.i - i, ij.j).occupied ||
         grid->Access(ij.i - i, ij.j).locked )
    {
      stop = true;
      continue;
    }

    neighbors.pts.push_back( nest_Pos(ij.i - i, ij.j) );
    neighbors.iNeg.push_back( nest_Pos(ij.i - i, ij.j) );
    ++i;
  }
  while ( !stop );

  // j+
  j = 1;
  stop = false;
  do
  {
    if ( ij.j != grid->Ny )
      D_j += grid->CellSize;

    if ( j >= grid->Ny - ij.j )
    {
      stop = true;
      continue;
    }

    if ( grid->Access(ij.i, ij.j + j).occupied ||
         grid->Access(ij.i, ij.j + j).locked )
    {
      stop = true;
      continue;
    }

    neighbors.pts.push_back( nest_Pos(ij.i, ij.j + j) );
    neighbors.jPos.push_back( nest_Pos(ij.i, ij.j + j) );
    ++j;
  }
  while ( !stop );

  // j-
  j = 1;
  stop = false;
  do
  {
    if ( ij.j != 0 )
      D_j += grid->CellSize;

    if ( (ij.j - j) < 0 )
    {
      stop = true;
      continue;
    }

    if ( grid->Access(ij.i, ij.j - j).occupied ||
         grid->Access(ij.i, ij.j - j).locked )
    {
      stop = true;
      continue;
    }

    neighbors.pts.push_back( nest_Pos(ij.i, ij.j - j) );
    neighbors.jNeg.push_back( nest_Pos(ij.i, ij.j - j) );
    ++j;
  }
  while ( !stop );

  neighbors.D_i = D_i;
  neighbors.D_j = D_j;
}

//-----------------------------------------------------------------------------

bool nest_BLF::isCursorPromising(const nest_Nbh&                        cursor,
                                 const std::vector< t_ptr<nest_Part> >& variants,
                                 std::vector< t_ptr<nest_Part> >&       result) const
{
  const double magic = 0.8;

  for ( const auto& variant : variants )
  {
    /* ====================
     *  Check bounding box.
     * ==================== */

    const tl::optional<core_BBox2d>& bbox = variant->GetBBox();

    t_uv         D_bbox   = bbox->maxPt - bbox->minPt;
    const double D_bbox_x = D_bbox.U()*magic;
    const double D_bbox_y = D_bbox.V()*magic;

    if ( (cursor.D_i < D_bbox_x) || (cursor.D_j < D_bbox_y) )
      continue;

    /* =====================
     *  Check point pattern.
     * ===================== */

    const t_ptr<SheetGrid>&      grid    = cursor.container->GetGrid();
    const std::vector<nest_Pos>& pattern = variant->GetPattern();

    // Check if every point from the pattern is vacant in the grid.
    bool isPatternOk = true;
    //
    for ( const auto& pt : pattern )
    {
      const int ii = cursor.origin.i + pt.i;
      const int jj = cursor.origin.j + pt.j;

      if ( ii >= grid->Nx || jj >= grid->Ny )
      {
        isPatternOk = false; // Part's feature point goes out of container.
        break;
      }

      nest_Pos probe({ii, jj});

      if ( grid->Access(probe.i, probe.j).occupied ||
           grid->Access(probe.i, probe.j).locked )
      {
        continue; // Part's feature point is already occupied if moved here.
      }
    }

    if ( !isPatternOk )
      continue;

    // This variant looks promising.
    result.push_back(variant);
  }

  return !result.empty();
}

//-----------------------------------------------------------------------------

t_ptr<nest_Part>
  nest_BLF::chooseBest(const std::vector< t_ptr<nest_Part> >& variants) const
{
  // Assess the bounding rect areas of all variants.
  std::vector<double> areas;
  std::vector<size_t> ids;
  //
  for ( size_t vidx = 0; vidx < variants.size(); ++vidx )
  {
    const tl::optional<core_BBox2d>& bbox = variants[vidx]->GetBBox();

    const double area = ( bbox->maxPt.V() - bbox->minPt.V() )
                      * ( bbox->maxPt.U() - bbox->minPt.U() );

    areas.push_back(area);
    ids  .push_back(vidx);
  }

  // Sort variants by descending bbox area (hence smaller material consumption).
  std::stable_sort( ids.begin(), ids.end(),
                    [&](const size_t a, const size_t b)
                    {
                      return areas[a] > areas[b] + core_Precision::Resolution3D();
                    } );

  const size_t bestIdx = ids[ids.size() - 1];

  return variants[bestIdx];
}

//-----------------------------------------------------------------------------

void nest_BLF::drawCursor(const nest_Nbh& cursor)
{
  const t_ptr<SheetGrid>& grid = cursor.container->GetGrid();
  //
  nest_GridPt& origin = grid->Access(cursor.origin.i, cursor.origin.j);
  t_uv         dU     = t_uv(cursor.D_i, 0);
  t_uv         dV     = t_uv(0, cursor.D_j);

  m_plotter.DRAW_AXES(origin.P, dU, dV,
                      cursor.isRoomy ? MobiusColor_Green : MobiusColor_Red,
                     "cursor");
}
