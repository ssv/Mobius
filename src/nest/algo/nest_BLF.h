//-----------------------------------------------------------------------------
// Created on: 27 January 2025
//-----------------------------------------------------------------------------
// Copyright (c) 2025-present, Sergey Slyadnev
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
//    * Redistributions of source code must retain the above copyright
//      notice, this list of conditions and the following disclaimer.
//    * Redistributions in binary form must reproduce the above copyright
//      notice, this list of conditions and the following disclaimer in the
//      documentation and/or other materials provided with the distribution.
//    * Neither the name of Sergey Slyadnev nor the
//      names of all contributors may be used to endorse or promote products
//      derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------

#ifndef nest_BLF_h
#define nest_BLF_h

// nest includes
#include <mobius/nest_Nbh.h>
#include <mobius/nest_Part.h>
#include <mobius/nest_Pos.h>
#include <mobius/nest_Sheet.h>

// geom includes
#include <mobius/geom_IAlgorithm.h>

//-----------------------------------------------------------------------------

namespace mobius {

//! \ingroup MOBIUS_NEST
//!
//! BLF (Bottom-Left-Fill) method is a heuristic way to arrange parts
//! in a container. This way people initialize the scene before running a
//! somewhat heavy optimization (no matter which one). The idea of BLF is pretty
//! much just moving parts w.r.t. each other so that they do not collide.
//!
//! This algorithm is completely stateless. This means that it does not store
//! previously placed parts but instead updates the target container. It is the
//! container that keeps the placed parts as well as the grid points' statuses.
//! You may freely construct instances of this BLF class for each placement request,
//! as long as the target container stays shared.
//!
//! \sa https://quaoar.su/blog/category/nesting
//!
//! CAUTION: Do not use the results of BLF as a solution for production nesting problem.
//!          The best use of this algorithm is to generate an initial guess for
//!          optimization.
class nest_BLF : public geom_IAlgorithm
{
public:

  //! Constructs the algorithm without a container. The container can be
  //! passed in later.
  mobiusNest_EXPORT
    nest_BLF(core_ProgressEntry progress = nullptr,
             geom_PlotterEntry  plotter  = nullptr);

  //! Initializes the algorithm with a container.
  mobiusNest_EXPORT
    nest_BLF(const t_ptr<nest_Sheet>& container,
             core_ProgressEntry       progress = nullptr,
             geom_PlotterEntry        plotter  = nullptr);\

public:

  //! Initializes the algorithm with a container.
  mobiusNest_EXPORT void
    Init(const t_ptr<nest_Sheet>& container);

  //! Places the passed part in the provided container.
  //! \param[in]  prototype     the "prototype" of the part to place.
  //! \param[out] nestedVariant the nested variant of the prototype.
  //! \param[out] elapsedTime   the elapsed time (in seconds).
  //! \param[out] numColTests   the number of collision tests applied.
  //! \return false if the part cannot be placed, i.e., there
  //!         is no sufficient space found by BLF in the container.
  mobiusNest_EXPORT bool
    PlacePart(const t_ptr<nest_Part>& prototype,
              t_ptr<nest_Part>&       nestedVariant,
              double&                 elapsedTime,
              int&                    numColTests);

protected:

  //! Creates as many copies of the part `part` as many rotated versions
  //! of it we will be attempting to nest. The prepared copies are all
  //! transformed properly.
  mobiusNest_EXPORT void
    prepareVariants(const t_ptr<nest_Part>&          part,
                    std::vector< t_ptr<nest_Part> >& variants) const;

  //! Populates point patterns for the passed variants.
  mobiusNest_EXPORT void
    preparePatterns(std::vector< t_ptr<nest_Part> >& variants) const;

  //! Checks if the already placed parts collide with the passed
  //! candidate part in the given variants. The collision-free
  //! variants are collected in the `result` vector.
  mobiusNest_EXPORT bool
    haveCollisions(const std::vector< t_ptr<nest_Part> >& variants,
                   std::vector< t_ptr<nest_Part> >&       result) const;

  //! Adds the passed part to the collection of already nested parts.
  mobiusNest_EXPORT void
    addNested(const t_ptr<nest_Part>& part);

  //! Collects all grid points occupied by the given part. This method
  //! takes the point pattern precomputed for a part and moves it by
  //! the given cursor position `pos`.
  mobiusNest_EXPORT void
    getSpannedPoints(const t_ptr<nest_Part>& part,
                     std::vector<nest_Pos>&  pts) const;

  //! Collects neighborhood for the given position.
  mobiusNest_EXPORT void
    getNeighborhood(const nest_Pos& ij,
                    nest_Nbh&       neighbors) const;

  //! Checks if the passed cursor position has a large enough neighborhood
  //! to emplace the part being nested in the given variants. All variants
  //! passing this filter are returned in the `result` collection.
  mobiusNest_EXPORT bool
    isCursorPromising(const nest_Nbh&                        cursor,
                      const std::vector< t_ptr<nest_Part> >& variants,
                      std::vector< t_ptr<nest_Part> >&       result) const;

  //! Selects the "best" variant among the passed ones.
  mobiusNest_EXPORT t_ptr<nest_Part>
    chooseBest(const std::vector< t_ptr<nest_Part> >& variants) const;

protected:

  //! Diagnostics function to draw the current position of the cursor.
  //! Does nothing in the default implementation. You can subclass `BLF`
  //! and implement the desired visualization in this callback.
  mobiusNest_EXPORT virtual void
    drawCursor(const nest_Nbh& cursor);

protected:

  t_ptr<nest_Sheet> m_container; //!< Target container.

};

} // mobius

#endif
