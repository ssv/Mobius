//-----------------------------------------------------------------------------
// Created on: 24 January 2025
//-----------------------------------------------------------------------------
// Copyright (c) 2013-present, Sergey Slyadnev
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
//    * Redistributions of source code must retain the above copyright
//      notice, this list of conditions and the following disclaimer.
//    * Redistributions in binary form must reproduce the above copyright
//      notice, this list of conditions and the following disclaimer in the
//      documentation and/or other materials provided with the distribution.
//    * Neither the name of Sergey Slyadnev nor the
//      names of all contributors may be used to endorse or promote products
//      derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------

// Own include
#include <mobius/nest_JSON.h>

using namespace mobius;

#define IL_BASENAME "il"

//-----------------------------------------------------------------------------

nest_JSON::nest_JSON() : core_JSON()
{}

//-----------------------------------------------------------------------------

nest_JSON::nest_JSON(const std::string& json) : core_JSON(json)
{}

//-----------------------------------------------------------------------------

nest_JSON::~nest_JSON()
{}

//-----------------------------------------------------------------------------

void nest_JSON::DumpPart(const t_ptr<nest_Part>& part)
{
  std::stringstream out;

  const t_ptr<t_polygon>&                ol = part->GetOuterLoop();
  const std::vector< t_ptr<t_polygon> >& il = part->GetInnerLoops();

  out << "{\n";
  out << "  \"name\": \"" << part->GetName() << "\",\n";
  out << "  \"outerPolygon\": " << ol->SerializePoles() << ",\n";
  out << "  \"innerPolygons\": " << "{\n";
  //
  for ( int k = 0; k < int( il.size() ); ++k )
  {
    out << "    \"" << std::string(IL_BASENAME) + core::str::to_string(k) << "\": ";
    out << il[k]->SerializePoles();

    if ( k < int( il.size() ) - 1 )
      out << ",\n";
  }
  //
  out << "\n";
  out << "  },\n";
  out << "  \"count\": " << part->GetCount() << ",\n";
  out << "  \"angleStepDeg\": " << part->GetAngleStepDeg() << "\n";
  out << "}";
 
  // Initialize json.
  m_json = out.str();
}

//-----------------------------------------------------------------------------

bool nest_JSON::ExtractPart(t_ptr<nest_Part>& part) const
{
  part = new nest_Part;

  // Part name.
  std::string name;
  this->ExtractBlockForKey("name", name);
  //
  part->SetName(name);

  // Outer polygon.
  std::vector<t_uv> outerPoles;
  this->ExtractVector2d("outerPolygon", outerPoles);
  //
  part->SetOuterLoop( new t_polygon(outerPoles) );
  part->GetOuterLoop()->Close(); // Make sure it's closed.

  // Inner polygons.
  std::vector< std::vector<t_uv> > innerPolesGrid;
  this->ExtractGrid2d("innerPolygons", innerPolesGrid);
  //
  for ( const auto& innerPoles : innerPolesGrid )
  {
    t_ptr<t_polygon> il = new t_polygon(innerPoles);
    il->Close(); // Make sure it's closed.
    //
    part->AddInnerLoop(il);
  }

  // Count.
  int count = 0;
  this->ExtractNumericBlockForKey<int>("count", count);
  //
  part->SetCount(count);

  // Angle.
  double angle = 0;
  this->ExtractNumericBlockForKey<double>("angleStepDeg", angle);
  //
  part->SetAngleStepDeg(angle);

  return true;
}
