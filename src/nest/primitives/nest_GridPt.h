//-----------------------------------------------------------------------------
// Created on: 23 January 2025
//-----------------------------------------------------------------------------
// Copyright (c) 2025-present, Sergey Slyadnev
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
//    * Redistributions of source code must retain the above copyright
//      notice, this list of conditions and the following disclaimer.
//    * Redistributions in binary form must reproduce the above copyright
//      notice, this list of conditions and the following disclaimer in the
//      documentation and/or other materials provided with the distribution.
//    * Neither the name of Sergey Slyadnev nor the
//      names of all contributors may be used to endorse or promote products
//      derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------

#ifndef nest_GridPt_h
#define nest_GridPt_h

// Nesting includes
#include <mobius/nest.h>

// Core includes
#include <mobius/core_UV.h>

//-----------------------------------------------------------------------------

namespace mobius {

//! \ingroup MOBIUS_NEST
//!
//! Grid point in a 2D nesting container.
struct nest_GridPt
{
  bool occupied; //!< Indicates whether this point is occupied by a part or not.
  bool locked;   //!< Indicates whether this point is temporarily locked by a nester.
  t_uv P;        //!< Grid point's coordinates (cached).

  nest_GridPt() : occupied(false), locked(false) {} //!< Default ctor.

  //! Ctor with coordinates.
  nest_GridPt(const double x,
              const double y)
  //
  : occupied ( false ),
    locked   ( false ),
    P        ( t_uv(x, y) )
  {}
};

} // mobius

#endif
