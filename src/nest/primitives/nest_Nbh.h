//-----------------------------------------------------------------------------
// Created on: 27 January 2025
//-----------------------------------------------------------------------------
// Copyright (c) 2025-present, Sergey Slyadnev
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
//    * Redistributions of source code must retain the above copyright
//      notice, this list of conditions and the following disclaimer.
//    * Redistributions in binary form must reproduce the above copyright
//      notice, this list of conditions and the following disclaimer in the
//      documentation and/or other materials provided with the distribution.
//    * Neither the name of Sergey Slyadnev nor the
//      names of all contributors may be used to endorse or promote products
//      derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------

#ifndef nest_Nbh_h
#define nest_Nbh_h

// Nesting includes
#include <mobius/nest_Pos.h>
#include <mobius/nest_Sheet.h>

// Geom includes
#include <mobius/geom_IPlotter.h>

//-----------------------------------------------------------------------------

namespace mobius {

//! \ingroup MOBIUS_NEST
//!
//! The neighborhood of a grid point used for part placement.
struct nest_Nbh
{
  t_ptr<nest_Sheet>     container;
  nest_Pos              origin; //!< Center point of the neighborhood.
  std::vector<nest_Pos> pts;
  std::vector<nest_Pos> iPos, iNeg;
  std::vector<nest_Pos> jPos, jNeg;
  bool                  isRoomy; //!< If there seems to be enough room to try nesting.
  double                D_i, D_j;

  //! Default ctor.
  nest_Nbh() : isRoomy(false), D_i(0.), D_j(0.) {}

  //! \return true if this neighborhood is empty.
  bool IsEmpty() const
  {
    return this->iPos.empty() && this->iNeg.empty() &&
           this->jPos.empty() && this->jNeg.empty();
  }
};

} // mobius

#endif
