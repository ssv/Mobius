//-----------------------------------------------------------------------------
// Created on: 23 January 2025
//-----------------------------------------------------------------------------
// Copyright (c) 2025-present, Sergey Slyadnev
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
//    * Redistributions of source code must retain the above copyright
//      notice, this list of conditions and the following disclaimer.
//    * Redistributions in binary form must reproduce the above copyright
//      notice, this list of conditions and the following disclaimer in the
//      documentation and/or other materials provided with the distribution.
//    * Neither the name of Sergey Slyadnev nor the
//      names of all contributors may be used to endorse or promote products
//      derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------

// Own include
#include <mobius/nest_Part.h>

// nest includes
#include <mobius/nest_JSON.h>
#include <mobius/nest_Sheet.h>

// Standard includes
#include <fstream>
#include <math.h>

using namespace mobius;

//-----------------------------------------------------------------------------

t_ptr<nest_Part>
  nest_Part::Import(const std::string& filename)
{
  t_ptr<nest_Part> part;

  std::ifstream FILE(filename);

  if ( !FILE.is_open() )
    return part;

  std::stringstream buffer;
  buffer << FILE.rdbuf();

  // Construct the part.
  nest_JSON interop( buffer.str() );
  //
  interop.ExtractPart(part);

  FILE.close();
  return part;
}

//-----------------------------------------------------------------------------

bool nest_Part::Export(const t_ptr<nest_Part>& part,
                       const std::string&      filename)
{
  std::ofstream FILE;
  FILE.open(filename, std::ios::out | std::ios::trunc);
  //
  if ( !FILE.is_open() )
  {
    return false;
  }

  // Dump part.
  nest_JSON interop;
  //
  interop.DumpPart(part);

  FILE << interop.GetJSON();

  FILE.close();
  return true;
}

//-----------------------------------------------------------------------------

nest_Part::nest_Part()
//
: core_PolygonPart (),
  m_iCount         (1),
  m_fAngStepDeg    (0.),
  m_status         (Pending)
{}

//-----------------------------------------------------------------------------

nest_Part::nest_Part(const t_ptr<t_polygon>&                outer,
                     const std::vector< t_ptr<t_polygon> >& inner)
//
: core_PolygonPart (outer, inner),
  m_iCount         (1),
  m_fAngStepDeg    (0.),
  m_status         (Pending)
{}

//-----------------------------------------------------------------------------

nest_Part::~nest_Part()
{}

//-----------------------------------------------------------------------------

t_ptr<core_PolygonPart> nest_Part::Copy() const
{
  t_ptr<nest_Part> copy = new nest_Part;

  // Copy loops.
  copy->m_outer = m_outer->Copy();
  //
  for ( auto& il : m_holes )
  {
    copy->m_holes.push_back( il->Copy() );
  }

  // Copy props.
  copy->m_iCount      = m_iCount;
  copy->m_fAngStepDeg = m_fAngStepDeg;

  // Copy container.
  copy->m_container = m_container;

  return copy.Access();
}

//-----------------------------------------------------------------------------

t_ptr<nest_Part> nest_Part::CopyRotated(const double angDeg) const
{
  t_ptr<nest_Part>
    copy = t_ptr<nest_Part>::DownCast( this->Copy() );

  if ( angDeg )
  {
    t_uv cog = this->ComputeCenter();

    t_trsf R ( t_qn(t_xyz::OZ(), angDeg*M_PI/180.), t_xyz() );
    t_trsf T1( t_qn(),                              t_xyz(-cog.U(), -cog.V(), 0.) );
    t_trsf T2( t_qn(),                              t_xyz( cog.U(),  cog.V(), 0.) );

    core_IsoTransformChain C;
    C << T2;
    C << R;
    C << T1;

    copy->Move(C);
  }

  return copy;
}

//-----------------------------------------------------------------------------

void nest_Part::ComputeBBox()
{
  double uMin, uMax, vMin, vMax;
  this->GetBounds(uMin, uMax, vMin, vMax);

  m_bbox        = core_BBox2d(uMin, vMin, uMax, vMax);
  m_anchorPoint = m_bbox->minPt; // Initialize anchor point.
}

//-----------------------------------------------------------------------------

void nest_Part::MoveTo(const nest_Pos& pos)
{
  if ( m_container.IsNull() )
    return;

  if ( !m_anchorPoint.has_value() )
    return;

  nest_Sheet*
    pSheet = dynamic_cast<nest_Sheet*>( m_container.Access() );
  //
  if ( !pSheet )
    return;

  const t_ptr<SheetGrid>& grid = pSheet->GetGrid();
  //
  if ( grid.IsNull() )
    return;

  t_uv T = grid->Access(pos.i, pos.j).P - *m_anchorPoint;

  // Transform poles.
  this->Move(T);

  // Transform anchor point.
  m_anchorPoint = *m_anchorPoint + T;
}

//-----------------------------------------------------------------------------

bool nest_Part::MoveBy(const t_uv& V)
{
  if ( !m_anchorPoint.has_value() )
    return false;

  // Transform poles.
  this->Move(V);

  // Transform anchor point.
  m_anchorPoint = *m_anchorPoint + V;

  // Transform bbox.
  if ( m_bbox.has_value() )
  {
    const double minU = m_bbox->minPt.U();
    const double minV = m_bbox->minPt.V();
    const double maxU = m_bbox->maxPt.U();
    const double maxV = m_bbox->maxPt.V();

    m_bbox = core_BBox2d( minU + V.U(), minV + V.V(), maxU + V.U(), maxV + V.V() );
  }

  return true;
}

//-----------------------------------------------------------------------------

void nest_Part::MovePatternTo(const nest_Pos& pos)
{
  // Transform pattern points.
  for ( auto& pt : m_pattern )
  {
    pt.i += pos.i;
    pt.j += pos.j;
  }
}

//-----------------------------------------------------------------------------

void nest_Part::CleanPattern()
{
  m_pattern.clear();
}
