//-----------------------------------------------------------------------------
// Created on: 22 January 2025
//-----------------------------------------------------------------------------
// Copyright (c) 2025-present, Sergey Slyadnev
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
//    * Redistributions of source code must retain the above copyright
//      notice, this list of conditions and the following disclaimer.
//    * Redistributions in binary form must reproduce the above copyright
//      notice, this list of conditions and the following disclaimer in the
//      documentation and/or other materials provided with the distribution.
//    * Neither the name of Sergey Slyadnev nor the
//      names of all contributors may be used to endorse or promote products
//      derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------

#ifndef nest_Part_HeaderFile
#define nest_Part_HeaderFile

// Nesting includes
#include <mobius/nest_Pos.h>

// Core includes
#include <mobius/core_BBox2d.h>
#include <mobius/core_Optional.h>
#include <mobius/core_PolygonPart.h>

namespace mobius {

//! \ingroup MOBIUS_NEST
//!
//! 2D part for nesting. This is a shared object representing a series of polygons,
//! one being the outer contour and optional child contours for holes.
//!
//! \sa core_PolygonPart
class nest_Part : public core_PolygonPart
{
public:

  //! Status of the part w.r.t. the nesting workflow.
  enum Status
  {
    Pending,   //!< Nesting yet to be performed.
    Nested,    //!< Already nested.
    Unnestable //!< No way to nest this part.
  };

// Serialization & deserialization:
public:

  //! Reads a part from the provided file.
  //! \param[in] filename the input filename.
  //! \return the constructed part.
  mobiusNest_EXPORT static t_ptr<nest_Part>
    Import(const std::string& filename);

  //! Exports the passed part to a file with the given name.
  //! \param[in] part     the part to export.
  //! \param[in] filename the target filename.
  //! \return true if worked.
  mobiusNest_EXPORT static bool
    Export(const t_ptr<nest_Part>& part,
           const std::string&      filename);

// Construction & destruction:
public:

  //! Ctor.
  mobiusNest_EXPORT
    nest_Part();

  //! Ctor with the contour polygons.
  //! \param[in] outer the outer polygon.
  //! \param[in] inner the inner polygons to represent holes and cutouts.
  mobiusNest_EXPORT
    nest_Part(const t_ptr<t_polygon>&                outer,
              const std::vector< t_ptr<t_polygon> >& inner = std::vector< t_ptr<t_polygon> >());

  //! Dtor.
  mobiusNest_EXPORT virtual
    ~nest_Part();

public:

  //! Sets the number of occurrences for this part.
  void SetCount(const int count)
  {
    m_iCount = count;
  }

  //! \return the stored num. of occurrences.
  int GetCount() const
  {
    return m_iCount;
  }

  //! Sets the allowed angular increment.
  void SetAngleStepDeg(const double ang)
  {
    m_fAngStepDeg = ang;
  }

  //! \return the allowed angular increment.
  double GetAngleStepDeg() const
  {
    return m_fAngStepDeg;
  }

  //! \return true if a point pattern is defined for this part.
  bool HasPattern() const
  {
    return !m_pattern.empty();
  }

  //! \return the stored point pattern.
  const std::vector<nest_Pos>& GetPattern() const
  {
    return m_pattern;
  }

  //! Sets the pattern to store.
  //! \param[in] pattern the pattern to store.
  void SetPattern(const std::vector<nest_Pos>& pattern)
  {
    m_pattern = pattern;
  }

  //! Adds the passed position to the stored pattern.
  //! \param[in] pos the position to add.
  void AddPatternPoint(const nest_Pos& pos)
  {
    m_pattern.push_back(pos);
  }

  //! \return true if the anchor point is initialized for this part.
  bool HasAnchorPoint() const
  {
    return m_anchorPoint.has_value();
  }

  //! \return the optional part's docking ("anchor") point.
  const tl::optional<t_uv>& GetAnchorPoint() const
  {
    return m_anchorPoint;
  }

  //! Sets the part's anchor point.
  //! \param[in] pt the point to set.
  void SetAnchorPoint(const tl::optional<t_uv>& pt)
  {
    m_anchorPoint = pt;
  }

  //! \return true if the bbox is initialized for this part.
  bool HasBBox() const
  {
    return m_bbox.has_value();
  }

  //! \return the optional part's bounding box.
  const tl::optional<core_BBox2d>& GetBBox() const
  {
    return m_bbox;
  }

  //! Sets the part's bbox.
  //! \param[in] bbox the bounding box to set.
  void SetBBox(const tl::optional<core_BBox2d>& bbox)
  {
    m_bbox = bbox;
  }

  //! \return true if the nesting container is set.
  bool HasContainer() const
  {
    return !m_container.IsNull();
  }

  //! \return the optional nesting container.
  const t_ptr<core_OBJECT>& GetContainer() const
  {
    return m_container;
  }

  //! Sets the nesting container.
  //! \param[in] container the container to set.
  void SetContainer(const t_ptr<core_OBJECT>& container)
  {
    m_container = container;
  }

  //! \return the last nesting position for this part.
  const nest_Pos& GetLastNestedPos() const
  {
    return m_lastNestedPos;
  }

  //! Sets the last nesting position for this part.
  //! \param[in] pos the last nesting position to set.
  void SetLastNestedPos(const nest_Pos& pos)
  {
    m_lastNestedPos = pos;
  }

  //! \return status w.r.t. nesting.
  Status GetStatus() const
  {
    return m_status;
  }

  //! Sets the status of this part w.r.t. nesting.
  //! \param[in] status the status to set.
  void SetStatus(const Status status)
  {
    m_status = status;
  }

  //! \return true if this part has a non-null prototype pointer.
  bool HasPrototype() const
  {
    return !m_prototype.IsNull();
  }

  //! \return the prototype of this part (if any).
  const t_ptr<nest_Part>& GetPrototype() const
  {
    return m_prototype;
  }

  //! \return non-const reference to the prototype of this part (if any).
  t_ptr<nest_Part>& ChangePrototype()
  {
    return m_prototype;
  }

  //! Sets the optional prototype for this part.
  //! \param[in] prototype the prototype to set.
  void SetPrototype(const t_ptr<nest_Part>& prototype)
  {
    m_prototype = prototype;
  }

public:

  //! \return deep copy of this part.
  mobiusNest_EXPORT virtual t_ptr<core_PolygonPart>
    Copy() const override;

  //! Creates a rotated copy of this part.
  //! \param[in] angDeg the angle to apply.
  //! \return deep copy of this part rotated by `angDeg` degrees.
  mobiusNest_EXPORT t_ptr<nest_Part>
    CopyRotated(const double angDeg) const;

  //! Computes and stores the bounding box of the part.
  mobiusNest_EXPORT void
    ComputeBBox();

  //! Moves the part to the position `pos` without its pattern points.
  mobiusNest_EXPORT void
    MoveTo(const nest_Pos& pos);

  //! Moves this part by the passed vector.
  mobiusNest_EXPORT bool
    MoveBy(const t_uv& V);

  //! Moves the part's pattern points to the position `pos`.
  mobiusNest_EXPORT void
    MovePatternTo(const nest_Pos& pos);

  //! Clears the stored pattern.
  mobiusNest_EXPORT void
    CleanPattern();

protected:

  int                       m_iCount;        //!< Num. of occurrences to nest.
  double                    m_fAngStepDeg;   //!< Step of allowed rotation.
  std::vector<nest_Pos>     m_pattern;       //!< Point pattern.
  tl::optional<t_uv>        m_anchorPoint;   //!< Part's docking point w.r.t. a BLF grid.
  tl::optional<core_BBox2d> m_bbox;          //!< Axis-aligned bounding box.
  t_ptr<core_OBJECT>        m_container;     //!< Back reference to the nesting container.
  nest_Pos                  m_lastNestedPos; //!< Last nesting position for this part.
  Status                    m_status;        //!< The part's status w.r.t. nesting.
  t_ptr<nest_Part>          m_prototype;     //!< Back reference to the prototype.

};

}

#endif
