//-----------------------------------------------------------------------------
// Created on: 23 January 2025
//-----------------------------------------------------------------------------
// Copyright (c) 2025-present, Sergey Slyadnev
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
//    * Redistributions of source code must retain the above copyright
//      notice, this list of conditions and the following disclaimer.
//    * Redistributions in binary form must reproduce the above copyright
//      notice, this list of conditions and the following disclaimer in the
//      documentation and/or other materials provided with the distribution.
//    * Neither the name of Sergey Slyadnev nor the
//      names of all contributors may be used to endorse or promote products
//      derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------

// Own include
#include <mobius/nest_Sheet.h>

using namespace mobius;

//-----------------------------------------------------------------------------

t_ptr<t_pcloud>
  nest_Sheet::GetPointCloud(const t_ptr<SheetGrid>& grid,
                            const int               filter)
{
  t_ptr<t_pcloud> pts = new t_pcloud;
  //
  for ( int j = 0; j < grid->Ny; ++j )
  {
    for ( int i = 0; i < grid->Nx; ++i )
    {
      bool addPt = false;
      if ( filter == PointFilter_All )
      {
        addPt = true;
      }
      else
      {
        const bool occupied = grid->Access(i, j).occupied;
        const bool locked   = grid->Access(i, j).locked;

        if ( (filter & PointFilter_Vacant) && (!occupied && !locked) )
          addPt = true;

        if ( (filter & PointFilter_Occupied) && occupied )
          addPt = true;

        if ( (filter & PointFilter_Locked) && locked )
          addPt = true;
      }

      if ( addPt )
        pts->AddPoint( grid->Access(i, j).P );
    }
  }

  return pts;
}

//-----------------------------------------------------------------------------

nest_Sheet::nest_Sheet()
//
: core_OBJECT (),
  m_fDx       (0.),
  m_fDy       (0.),
  m_fMoveX    (0.),
  m_fMoveY    (0.)
{
}

//-----------------------------------------------------------------------------

nest_Sheet::nest_Sheet(const double dx,
                       const double dy)
//
: core_OBJECT (),
  m_fDx       (dx),
  m_fDy       (dy),
  m_fMoveX    (0.),
  m_fMoveY    (0.)
{
}

//-----------------------------------------------------------------------------

nest_Sheet::~nest_Sheet()
{}

//-----------------------------------------------------------------------------

void nest_Sheet::Init(const double dx,
                      const double dy)
{
  m_fDx = dx;
  m_fDy = dy;
}

//-----------------------------------------------------------------------------

void nest_Sheet::GetBounds(double& xMin, double& xMax,
                           double& yMin, double& yMax) const
{
  xMin = yMin = 0;
  xMax = m_fDx;
  yMax = m_fDy;
}

//-----------------------------------------------------------------------------

t_ptr<SheetGrid>
  nest_Sheet::BuildGrid(const double step)
{
  // Number of cells in each dimension.
  const int nx = int( m_fDx / step ) + 1;
  const int ny = int( m_fDy / step ) + 1;

  // Initialize grid.
  m_grid = new SheetGrid(0., 0., nx, ny, step);

  // Populate grid.
  for ( int j = 0; j < ny; ++j )
  {
    const double y = step*j;

    for ( int i = 0; i < nx; ++i )
    {
      const double x = step*i;

      m_grid->Access(i, j) = nest_GridPt(x, y);
    }
  }

  return m_grid;
}

//-----------------------------------------------------------------------------

void nest_Sheet::BuildPointPattern(const t_ptr<nest_Part>& part) const
{
  // Set back reference to the container.
  part->SetContainer(this);

  // Compute the anchor point of the part.
  if ( !part->HasAnchorPoint() )
  {
    // Bbox computation initializes the anchor point.
    part->ComputeBBox();
  }

  // Move part to origin.
  part->MoveTo( {0, 0} );

  // Clean up previous pattern (if any).
  part->CleanPattern();

  // Classify grid points.
  for ( int i = 0; i < m_grid->Nx; ++i )
  {
    for ( int j = 0; j < m_grid->Ny; ++j )
    {
      if ( part->IsPointInside( m_grid->Access(i, j).P ) )
      {
        // w.r.t. anchor point.
        part->AddPatternPoint( {i, j} );
      }
    }
  }
}

//-----------------------------------------------------------------------------

void nest_Sheet::UnlockAll()
{
  for ( int i = 0; i < m_grid->Nx; ++i )
  {
    for ( int j = 0; j < m_grid->Ny; ++j )
    {
      m_grid->Access(i, j).locked = false;
    }
  }
}

//-----------------------------------------------------------------------------

void nest_Sheet::AddNested(const t_ptr<nest_Part>& part)
{
  m_nested.push_back(part);
}

//-----------------------------------------------------------------------------

t_ptr<t_polygonPart> nest_Sheet::GetShape() const
{
  t_ptr<t_polygon>
    ol = new t_polygon( {{m_fMoveX,         m_fMoveY},
                         {m_fMoveX + m_fDx, m_fMoveY},
                         {m_fMoveX + m_fDx, m_fMoveY + m_fDy},
                         {m_fMoveX,         m_fMoveY + m_fDy}} );
  //
  ol->Close();

  return new t_polygonPart(ol);
}

//-----------------------------------------------------------------------------

void nest_Sheet::ApplyVirtualMovement()
{
  for ( auto& I : m_nested )
  {
    I->MoveBy( t_uv(m_fMoveX, m_fMoveY) );
  }
}

