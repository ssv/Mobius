//-----------------------------------------------------------------------------
// Created on: 22 January 2025
//-----------------------------------------------------------------------------
// Copyright (c) 2025-present, Sergey Slyadnev
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
//    * Redistributions of source code must retain the above copyright
//      notice, this list of conditions and the following disclaimer.
//    * Redistributions in binary form must reproduce the above copyright
//      notice, this list of conditions and the following disclaimer in the
//      documentation and/or other materials provided with the distribution.
//    * Neither the name of Sergey Slyadnev nor the
//      names of all contributors may be used to endorse or promote products
//      derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------

#ifndef nest_Sheet_HeaderFile
#define nest_Sheet_HeaderFile

// Nesting includes
#include <mobius/nest_GridPt.h>
#include <mobius/nest_Part.h>
#include <mobius/nest_SheetGrid.h>

// Geom includes
#include <mobius/geom_PositionCloud.h>

namespace mobius {

//! \ingroup MOBIUS_NEST
//!
//! 2D rectangular sheet for nesting.
class nest_Sheet : public core_OBJECT
{
public:

  enum PointFilter
  {
    PointFilter_Vacant   = 0x01,
    PointFilter_Occupied = 0x02,
    PointFilter_Locked   = 0x04,
    PointFilter_All      = PointFilter_Vacant | PointFilter_Occupied | PointFilter_Locked
  };

public:

  //! Turns a uniform grid into a point cloud.
  mobiusNest_EXPORT static t_ptr<t_pcloud>
    GetPointCloud(const t_ptr<SheetGrid>& grid,
                  const int               filter = PointFilter_All);

// Construction & destruction:
public:

  //! Ctor.
  mobiusNest_EXPORT
    nest_Sheet();

  //! Ctor with dimensions.
  mobiusNest_EXPORT
    nest_Sheet(const double dx,
               const double dy);

  //! Dtor.
  mobiusNest_EXPORT virtual
    ~nest_Sheet();

public:

  //! Initializes the container's properties.
  mobiusNest_EXPORT void
    Init(const double dx,
         const double dy);

  //! Returns the two-dimensional bounds of the sheet.
  mobiusNest_EXPORT void
    GetBounds(double& xMin, double& xMax,
              double& yMin, double& yMax) const;

  //! Builds a structured point grid for the container.
  mobiusNest_EXPORT t_ptr<SheetGrid>
    BuildGrid(const double step);

  //! Builds point pattern for the passed part. The grid is not
  //! affected, while the `part` object is initialized with its
  //! pointwise representation w.r.t. the grid.
  mobiusNest_EXPORT void
    BuildPointPattern(const t_ptr<nest_Part>& part) const;

  //! Unlocks all points in the grid.
  mobiusNest_EXPORT void
    UnlockAll();

  //! Adds the passed part to the collection of nested parts.
  mobiusNest_EXPORT void
    AddNested(const t_ptr<nest_Part>& part);

  //! Turns this sheet into a polygonal part and returns it.
  mobiusNest_EXPORT t_ptr<t_polygonPart>
    GetShape() const;

  //! Applies the stored virtual movement to all the stored
  //! nested parts.
  mobiusNest_EXPORT void
    ApplyVirtualMovement();

public:

  //! \return true if this container has its uniform grid computed.
  bool HasGrid() const
  {
    return !m_grid.IsNull();
  }

  //! \return the structured grid of snapping points
  //!         stored in this container.
  const t_ptr<SheetGrid>& GetGrid() const
  {
    return m_grid;
  }

  //! \return the const reference to the nested parts.
  const std::vector< t_ptr<nest_Part> >& GetNested() const
  {
    return m_nested;
  }

  //! \return OX dimension of the container.
  double GetXDim() const
  {
    return m_fDx;
  }

  //! \return OY dimension of the container.
  double GetYDim() const
  {
    return m_fDy;
  }

  //! Sets the virtual movement to apply to this container.
  void SetVirtualMovement(const double dx,
                          const double dy)
  {
    m_fMoveX = dx;
    m_fMoveY = dy;
  }

  //! \return the virtual movement applied to this container.
  void GetVirtualMovement(double& dx,
                          double& dy)
  {
    dx = m_fMoveX;
    dy = m_fMoveY;
  }

  //! Adds the passed part to the collection of
  //! blacklisted prototypes.
  void AddBlacklisted(const t_ptr<nest_Part>& part)
  {
    m_blacklisted.push_back(part);
  }

  //! Checks if the passed part is blacklisted or not.
  bool IsBlacklisted(const t_ptr<nest_Part>& part) const
  {
    for ( const auto& p : m_blacklisted )
    {
      if ( p == part )
        return true;
    }

    return false;
  }

protected:

  double m_fDx; //!< OX dimension.
  double m_fDy; //!< OY dimension.

  //! Structured grid of points in a container.
  t_ptr<SheetGrid> m_grid;

  //! Nested parts.
  std::vector< t_ptr<nest_Part> > m_nested;

  //! Blacklisted prototypes, i.e., the instances of
  //! these prototypes could not be nested anymore.
  std::vector< t_ptr<nest_Part> > m_blacklisted;

  double m_fMoveX; //!< Virtual movement along OX.
  double m_fMoveY; //!< Virtual movement along OY.

};

}

#endif
