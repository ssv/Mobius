//-----------------------------------------------------------------------------
// Created on: 23 January 2025
//-----------------------------------------------------------------------------
// Copyright (c) 2025-present, Sergey Slyadnev
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
//    * Redistributions of source code must retain the above copyright
//      notice, this list of conditions and the following disclaimer.
//    * Redistributions in binary form must reproduce the above copyright
//      notice, this list of conditions and the following disclaimer in the
//      documentation and/or other materials provided with the distribution.
//    * Neither the name of Sergey Slyadnev nor the
//      names of all contributors may be used to endorse or promote products
//      derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------

#ifndef nest_SheetGrid_HeaderFile
#define nest_SheetGrid_HeaderFile

// Nesting includes
#include <mobius/nest_GridPt.h>

namespace mobius {

//! \ingroup MOBIUS_NEST
//!
//! 2D structured grid for nesting packed into a plain array.
template <typename TCoord, typename TValue>
class nest_SheetGrid : public core_OBJECT
{
public:

  //! Ctor.
  //! \param[in] xmin     min X.
  //! \param[in] ymin     min Y.
  //! \param[in] nx       num. of cells along OX axis.
  //! \param[in] ny       num. of cells along OY axis.
  //! \param[in] cellSize cell size.
  nest_SheetGrid(const TCoord xmin,
                 const TCoord ymin,
                 const int    nx,
                 const int    ny,
                 const TCoord cellSize)
  {
    XMin     = xmin;
    YMin     = ymin;
    Nx       = nx;
    Ny       = ny;
    M        = nx*ny;
    CellSize = cellSize;

    pArray = new TValue[M];
  }

  //! Dtor.
  ~nest_SheetGrid()
  {
    delete[] pArray;
  }

  TValue& Access(const int i,
                 const int j)
  {
    return pArray[i + Nx*j];
  }

  int FromPair(const int i,
               const int j)
  {
    return i + Nx*j;
  }

  void FromPlain(const int idx,
                 int&      i,
                 int&      j)
  {
    j = idx / Nx;
    i = idx - j*Nx;
  }

public:

  /* Min. corner. */
  TCoord XMin, YMin;

  /* Number of points in each direction. */
  int Nx, Ny;

  /* Array size. */
  int M;

  /* Cell size. */
  TCoord CellSize;

  /* Array of associated values. */
  TValue* pArray;
};

typedef nest_SheetGrid<double, nest_GridPt> SheetGrid;

}

#endif
