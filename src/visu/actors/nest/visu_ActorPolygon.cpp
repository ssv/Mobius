//-----------------------------------------------------------------------------
// Created on: 15 May 2024
//-----------------------------------------------------------------------------
// Copyright (c) 2024-present, Sergey Slyadnev
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
//    * Redistributions of source code must retain the above copyright
//      notice, this list of conditions and the following disclaimer.
//    * Redistributions in binary form must reproduce the above copyright
//      notice, this list of conditions and the following disclaimer in the
//      documentation and/or other materials provided with the distribution.
//    * Neither the name of Sergey Slyadnev nor the
//      names of all contributors may be used to endorse or promote products
//      derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------

// Windows
#include <windows.h>

// GL includes
#include <gl/gl.h>
#include <gl/glu.h>

// Own include
#include <mobius/visu_ActorPolygon.h>

// Core includes
#include <mobius/core_PointInPoly.h>

// Standard includes
#include <algorithm>
#include <math.h>

using namespace mobius;

#define SHIFT 0.05
#define ROTO  2.0

//-----------------------------------------------------------------------------

visu_ActorPolygon::visu_ActorPolygon(const t_ptr<core_Polygon>&    polygon,
                                     const visu_ColorRGB<GLubyte>& color)
: visu_Actor  (),
  m_bCtrlDown (false),
  m_fPickedU  (0.),
  m_fPickedV  (0.),
  m_fMoveStep (SHIFT)
{
  this->Init(polygon);
  this->SetColor(color);
}

//-----------------------------------------------------------------------------

visu_ActorPolygon::~visu_ActorPolygon()
{}

//-----------------------------------------------------------------------------

void visu_ActorPolygon::Init(const t_ptr<core_Polygon>& polygon)
{
  m_polygon = polygon;

  // Compute characterics step.
  double xMin, xMax, yMin, yMax;
  m_polygon->GetBounds(xMin, xMax, yMin, yMax);

  const double hx = xMax - xMin;
  const double hy = yMax - yMin;

  m_fMoveStep = hx > hy ? hx : hy;
  m_fMoveStep *= 0.025;
}

//-----------------------------------------------------------------------------

void visu_ActorPolygon::GetBounds(double& xMin, double& xMax,
                                  double& yMin, double& yMax,
                                  double& zMin, double& zMax) const
{
  m_polygon->GetBounds(xMin, xMax, yMin, yMax);
  //
  zMin = 0.0;
  zMax = 0.0;
}

//-----------------------------------------------------------------------------

void visu_ActorPolygon::GL_Draw()
{
  const std::vector<t_uv>& poles = m_polygon->GetPoles();
  //
  if ( poles.empty() )
    return;

  // Draw segments.
  glBegin(GL_LINES);
  glLineWidth(1.f);
  glEnable(GL_LINE_SMOOTH);
    glColor3ub(m_color.R, m_color.G, m_color.B);
    for ( size_t i = 0; i < poles.size() - 1; ++i )
    {
      const t_uv& P1 = poles[i];
      const t_uv& P2 = poles[i + 1];

      glVertex3f( (GLfloat) P1.U(), (GLfloat) P1.V(), 0.f );
      glVertex3f( (GLfloat) P2.U(), (GLfloat) P2.V(), 0.f );
    }
  glDisable(GL_LINE_SMOOTH);
  glEnd();

  // Draw extremity points.
  glPointSize(5);
  glBegin(GL_POINTS);
  glColor3ub(QrYellow.R, QrYellow.G, QrYellow.B);
    for ( size_t i = 0; i < poles.size(); ++i )
    {
      const t_uv& P = poles[i];

      glVertex3f( (GLfloat) P.U(), (GLfloat) P.V(), 0.f );
    }
  glEnd();
}

//-----------------------------------------------------------------------------

t_ptr<visu_DataSet>
  visu_ActorPolygon::IntersectWithLine(const t_ptr<geom_Line>& line)
{
  t_ptr<visu_DataSet> res;

  // Point to test.
  t_uv point( line->GetOrigin().X(), line->GetOrigin().Y() );
  //
  m_fPickedU = point.U();
  m_fPickedV = point.V();

  // PMC test.
  const bool isInside = m_polygon->IsPointInside(point);
  //
  if ( isInside )
  {
    res = new visu_DataPolygon( m_polygon->Copy() );
    std::cout << "Picked polygon: " << m_polygon->GetName() << std::endl;
  }

  return res;
}

//-----------------------------------------------------------------------------

void visu_ActorPolygon::ProcessMessage(UINT   message,
                                       WPARAM wparam,
                                       LPARAM)
{
  switch( message )
  {
    case WM_KEYDOWN:
    {
      switch ( wparam )
      {
        case VK_LEFT:
        {
          if ( m_bCtrlDown )
          {
            t_trsf R ( t_qn(t_xyz::OZ(), ROTO*M_PI/180.), t_xyz() );
            t_trsf T1( t_qn(),                            t_xyz(-m_fPickedU, -m_fPickedV, 0.) );
            t_trsf T2( t_qn(),                            t_xyz(m_fPickedU, m_fPickedV, 0.) );

            core_IsoTransformChain C;
            C << T2;
            C << R;
            C << T1;

            m_polygon                   ->Move(C);
            m_hiliPolygon->GetPolygon() ->Move(C);
          }
          else
          {
            m_polygon                   ->Move( t_uv(-m_fMoveStep, 0) );
            m_hiliPolygon->GetPolygon() ->Move( t_uv(-m_fMoveStep, 0) );
          }

          this->GL_Draw();
          this->GL_Hili();
          break;
        }
        case VK_UP:
        {
          m_polygon                   ->Move( t_uv(0, m_fMoveStep) );
          m_hiliPolygon->GetPolygon() ->Move( t_uv(0, m_fMoveStep) );
          //
          this->GL_Draw();
          this->GL_Hili();
          break;
        }
        case VK_RIGHT:
        {
          if ( m_bCtrlDown )
          {
            t_trsf R ( t_qn(t_xyz::OZ(), -ROTO*M_PI/180.), t_xyz() );
            t_trsf T1( t_qn(),                             t_xyz(-m_fPickedU, -m_fPickedV, 0.) );
            t_trsf T2( t_qn(),                             t_xyz(m_fPickedU, m_fPickedV, 0.) );

            core_IsoTransformChain C;
            C << T2;
            C << R;
            C << T1;

            m_polygon                   ->Move(C);
            m_hiliPolygon->GetPolygon() ->Move(C);
          }
          else
          {
            m_polygon                   ->Move( t_uv(m_fMoveStep, 0) );
            m_hiliPolygon->GetPolygon() ->Move( t_uv(m_fMoveStep, 0) );
          }

          this->GL_Draw();
          this->GL_Hili();
          break;
        }
        case VK_DOWN:
        {
          m_polygon                   ->Move( t_uv(0, -m_fMoveStep) );
          m_hiliPolygon->GetPolygon() ->Move( t_uv(0, -m_fMoveStep) );
          //
          this->GL_Draw();
          this->GL_Hili();
          break;
        }
        case VK_CONTROL:
        {
          m_bCtrlDown = true;
          break;
        }
        default:
          break;
      }
      return;
    }
    case WM_KEYUP:
    {
      switch( wparam )
      {
        case VK_CONTROL:
        {
          m_bCtrlDown = false;
          break;
        }
        default:
          break;
      }
      return;
    }
    default:
      break;
  }
}

//-----------------------------------------------------------------------------

void visu_ActorPolygon::SetHiliData(const t_ptr<visu_DataSet>& data)
{
  m_hiliPolygon = t_ptr<visu_DataPolygon>::DownCast(data);
}

//-----------------------------------------------------------------------------

t_ptr<visu_DataSet> visu_ActorPolygon::GetHiliData() const
{
  return m_hiliPolygon;
}

//-----------------------------------------------------------------------------

void visu_ActorPolygon::ClearHiliData()
{
  if ( !m_hiliPolygon.IsNull() )
    m_hiliPolygon->Clear();
}

//-----------------------------------------------------------------------------

void visu_ActorPolygon::GL_Hili() const
{
  if ( m_hiliPolygon.IsNull() )
    return;

  const std::vector<t_uv>& poles = m_hiliPolygon->GetPolygon()->GetPoles();
  //
  if ( poles.empty() )
    return;

  // Draw segments.
  glBegin(GL_LINES);
  glLineWidth(2.f);
  glEnable(GL_LINE_SMOOTH);
    glColor3ub(QrLightBlue.R, QrLightBlue.G, QrLightBlue.B);
    for ( size_t i = 0; i < poles.size() - 1; ++i )
    {
      const t_uv& P1 = poles[i];
      const t_uv& P2 = poles[i + 1];

      glVertex3f( (GLfloat) P1.U(), (GLfloat) P1.V(), 0.f );
      glVertex3f( (GLfloat) P2.U(), (GLfloat) P2.V(), 0.f );
    }
  glDisable(GL_LINE_SMOOTH);
  glEnd();
}

//-----------------------------------------------------------------------------

void visu_ActorPolygon::SetColor(const visu_ColorRGB<GLubyte>& color)
{
  m_color = color;
}
