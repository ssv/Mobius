//-----------------------------------------------------------------------------
// Created on: 15 May 2024
//-----------------------------------------------------------------------------
// Copyright (c) 2024-present, Sergey Slyadnev
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
//    * Redistributions of source code must retain the above copyright
//      notice, this list of conditions and the following disclaimer.
//    * Redistributions in binary form must reproduce the above copyright
//      notice, this list of conditions and the following disclaimer in the
//      documentation and/or other materials provided with the distribution.
//    * Neither the name of Sergey Slyadnev nor the
//      names of all contributors may be used to endorse or promote products
//      derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------

#ifndef visu_ActorPolygon_HeaderFile
#define visu_ActorPolygon_HeaderFile

// visu includes
#include <mobius/visu_Actor.h>
#include <mobius/visu_DataPolygon.h>

// core includes
#include <mobius/core_Polygon.h>

namespace mobius {

//! \ingroup MOBIUS_VISU
//!
//! Actor for a 2D nestable polygon.
class visu_ActorPolygon : public visu_Actor
{
public:

  //! Ctor.
  //! \param[in] polygon the polygon to draw.
  //! \param[in] color   the color to set.
  mobiusVisu_EXPORT
    visu_ActorPolygon(const t_ptr<core_Polygon>&          polygon,
                      const visu_ColorRGB<unsigned char>& color);

  //! Dtor.
  mobiusVisu_EXPORT virtual
    ~visu_ActorPolygon();

public:

  //! Initializes this actor.
  mobiusVisu_EXPORT void
    Init(const t_ptr<core_Polygon>& polygon);

  //! Calculates the axis-aligned bounding box of this actor.
  //! \param[out] xMin min X.
  //! \param[out] xMax max X.
  //! \param[out] yMin min Y.
  //! \param[out] yMax max Y.
  //! \param[out] zMin min Z.
  //! \param[out] zMax max Z.
  mobiusVisu_EXPORT virtual void
    GetBounds(double& xMin, double& xMax,
              double& yMin, double& yMax,
              double& zMin, double& zMax) const;

  //! Draws mesh.
  mobiusVisu_EXPORT virtual void
    GL_Draw();

public:

  mobiusVisu_EXPORT virtual t_ptr<visu_DataSet>
    IntersectWithLine(const t_ptr<geom_Line>&);

  mobiusVisu_EXPORT virtual void
    ProcessMessage(UINT   message,
                   WPARAM wparam,
                   LPARAM lparam);

  mobiusVisu_EXPORT virtual void
    SetHiliData(const t_ptr<visu_DataSet>&);

  mobiusVisu_EXPORT virtual t_ptr<visu_DataSet>
    GetHiliData() const;

  mobiusVisu_EXPORT virtual void
    ClearHiliData();

  mobiusVisu_EXPORT virtual void
    GL_Hili() const;

  mobiusVisu_EXPORT virtual void
    SetColor(const visu_ColorRGB<unsigned char>&);

protected:

  //! Polygon to draw.
  t_ptr<core_Polygon> m_polygon;

  //! Color.
  visu_ColorRGB<unsigned char> m_color;

  //! Data for highlighting.
  t_ptr<visu_DataPolygon> m_hiliPolygon;

  //! Indicates if the <Ctrl> key is down.
  bool m_bCtrlDown;

  //! Picked U coordinate.
  double m_fPickedU;

  //! Picked V coordinate.
  double m_fPickedV;

  //! Move step.
  double m_fMoveStep;

};

}

#endif
