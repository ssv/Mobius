//-----------------------------------------------------------------------------
// Created on: 24 January 2025
//-----------------------------------------------------------------------------
// Copyright (c) 2025-present, Sergey Slyadnev
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
//    * Redistributions of source code must retain the above copyright
//      notice, this list of conditions and the following disclaimer.
//    * Redistributions in binary form must reproduce the above copyright
//      notice, this list of conditions and the following disclaimer in the
//      documentation and/or other materials provided with the distribution.
//    * Neither the name of Sergey Slyadnev nor the
//      names of all contributors may be used to endorse or promote products
//      derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------

// Windows
#include <windows.h>

// GL includes
#include <gl/gl.h>
#include <gl/glu.h>

// Own include
#include <mobius/visu_ActorPolygonPart.h>

// Nesting includes
#include <mobius/nest_Sheet.h>

// Standard includes
#include <algorithm>
#include <math.h>

using namespace mobius;

#define SHIFT 0.05
#define ROTO  2.0

namespace
{
  void drawPolyline(const std::vector<t_uv>&      poles,
                    const visu_ColorRGB<GLubyte>& color,
                    const float                   lw)
  {
    if ( poles.empty() )
      return;

    // Draw segments.
    glBegin(GL_LINES);
    glLineWidth(lw);
    glEnable(GL_LINE_SMOOTH);
    glColor3ub(color.R, color.G, color.B);
      //
      for ( size_t i = 0; i < poles.size() - 1; ++i )
      {
        const t_uv& P1 = poles[i];
        const t_uv& P2 = poles[i + 1];

        glVertex3f( (GLfloat) P1.U(), (GLfloat) P1.V(), 0.f );
        glVertex3f( (GLfloat) P2.U(), (GLfloat) P2.V(), 0.f );
      }
      //
    glDisable(GL_LINE_SMOOTH);
    glEnd();

    // Draw extremity points.
    glPointSize(5);
    glBegin(GL_POINTS);
    glColor3ub(QrYellow.R, QrYellow.G, QrYellow.B);
      //
      for ( size_t i = 0; i < poles.size(); ++i )
      {
        const t_uv& P = poles[i];

        glVertex3f( (GLfloat) P.U(), (GLfloat) P.V(), 0.f );
      }
      //
    glEnd();
  }
}

//-----------------------------------------------------------------------------

visu_ActorPolygonPart::visu_ActorPolygonPart(const t_ptr<t_polygonPart>&   part,
                                             const visu_ColorRGB<GLubyte>& color)
: visu_Actor  (),
  m_bCtrlDown (false),
  m_fPickedU  (0.),
  m_fPickedV  (0.),
  m_fMoveStep (SHIFT)
{
  this->Init(part);
  this->SetColor(color);
}

//-----------------------------------------------------------------------------

visu_ActorPolygonPart::~visu_ActorPolygonPart()
{}

//-----------------------------------------------------------------------------

void visu_ActorPolygonPart::Init(const t_ptr<t_polygonPart>& part)
{
  m_part = part;

  // Compute characterics step.
  double xMin, xMax, yMin, yMax;
  m_part->GetBounds(xMin, xMax, yMin, yMax);

  const double hx = xMax - xMin;
  const double hy = yMax - yMin;

  m_fMoveStep = hx > hy ? hx : hy;
  m_fMoveStep *= 0.025;
}

//-----------------------------------------------------------------------------

void visu_ActorPolygonPart::GetBounds(double& xMin, double& xMax,
                                      double& yMin, double& yMax,
                                      double& zMin, double& zMax) const
{
  m_part->GetBounds(xMin, xMax, yMin, yMax);
  //
  zMin = 0.0;
  zMax = 0.0;
}

//-----------------------------------------------------------------------------

void visu_ActorPolygonPart::GL_Draw()
{
  // Outer loop.
  ::drawPolyline(m_part->GetOuterLoop()->GetPoles(), m_color, 1.f);

  // Inner loops.
  const std::vector< t_ptr<t_polygon> >& il = m_part->GetInnerLoops();
  //
  for ( const auto& loop : il )
  {
    ::drawPolyline(loop->GetPoles(), m_color, 1.f);
  }

  nest_Part*
    pNestablePart = dynamic_cast<nest_Part*>( m_part.Access() );

  // Pattern points.
  if ( pNestablePart && pNestablePart->HasPattern() )
  {
    const std::vector<nest_Pos>& pattern = pNestablePart->GetPattern();

    if ( pNestablePart->HasContainer() )
    {
      t_ptr<nest_Sheet>
        sheet = t_ptr<nest_Sheet>::DownCast( pNestablePart->GetContainer() );

      // Draw pattern points.
      glPointSize(9);
      glBegin(GL_POINTS);
      glColor3ub(QrBlueish.R, QrBlueish.G, QrBlueish.B);
        //
        for ( const auto& pos : pattern )
        {
          const t_uv& P = sheet->GetGrid()->Access(pos.i, pos.j).P;

          glVertex3f( (GLfloat) P.U(), (GLfloat) P.V(), 0.f );
        }
        //
      glEnd();
    }
  }
}

//-----------------------------------------------------------------------------

t_ptr<visu_DataSet>
  visu_ActorPolygonPart::IntersectWithLine(const t_ptr<geom_Line>& line)
{
  t_ptr<visu_DataSet> res;

  // Point to test.
  t_uv point( line->GetOrigin().X(), line->GetOrigin().Y() );
  //
  m_fPickedU = point.U();
  m_fPickedV = point.V();

  // PMC test.
  const bool isInside = m_part->IsPointInside(point);
  //
  if ( isInside )
  {
    res = new visu_DataPolygonPart( m_part->Copy() );
    std::cout << "Picked part: " << m_part->GetName() << std::endl;
  }

  return res;
}

//-----------------------------------------------------------------------------

void visu_ActorPolygonPart::ProcessMessage(UINT   message,
                                           WPARAM wparam,
                                           LPARAM)
{
  switch( message )
  {
    case WM_KEYDOWN:
    {
      switch ( wparam )
      {
        case VK_LEFT:
        {
          if ( m_bCtrlDown )
          {
            t_trsf R ( t_qn(t_xyz::OZ(), ROTO*M_PI/180.), t_xyz() );
            t_trsf T1( t_qn(),                            t_xyz(-m_fPickedU, -m_fPickedV, 0.) );
            t_trsf T2( t_qn(),                            t_xyz(m_fPickedU, m_fPickedV, 0.) );

            core_IsoTransformChain C;
            C << T2;
            C << R;
            C << T1;

            m_part                ->Move(C);
            m_hiliPart->GetPart() ->Move(C);
          }
          else
          {
            m_part                ->Move( t_uv(-m_fMoveStep, 0) );
            m_hiliPart->GetPart() ->Move( t_uv(-m_fMoveStep, 0) );
          }

          this->GL_Draw();
          this->GL_Hili();
          break;
        }
        case VK_UP:
        {
          m_part                ->Move( t_uv(0, m_fMoveStep) );
          m_hiliPart->GetPart() ->Move( t_uv(0, m_fMoveStep) );
          //
          this->GL_Draw();
          this->GL_Hili();
          break;
        }
        case VK_RIGHT:
        {
          if ( m_bCtrlDown )
          {
            t_trsf R ( t_qn(t_xyz::OZ(), -ROTO*M_PI/180.), t_xyz() );
            t_trsf T1( t_qn(),                             t_xyz(-m_fPickedU, -m_fPickedV, 0.) );
            t_trsf T2( t_qn(),                             t_xyz(m_fPickedU, m_fPickedV, 0.) );

            core_IsoTransformChain C;
            C << T2;
            C << R;
            C << T1;

            m_part                ->Move(C);
            m_hiliPart->GetPart() ->Move(C);
          }
          else
          {
            m_part                ->Move( t_uv(m_fMoveStep, 0) );
            m_hiliPart->GetPart() ->Move( t_uv(m_fMoveStep, 0) );
          }

          this->GL_Draw();
          this->GL_Hili();
          break;
        }
        case VK_DOWN:
        {
          m_part                ->Move( t_uv(0, -m_fMoveStep) );
          m_hiliPart->GetPart() ->Move( t_uv(0, -m_fMoveStep) );
          //
          this->GL_Draw();
          this->GL_Hili();
          break;
        }
        case VK_CONTROL:
        {
          m_bCtrlDown = true;
          break;
        }
        default:
          break;
      }
      return;
    }
    case WM_KEYUP:
    {
      switch( wparam )
      {
        case VK_CONTROL:
        {
          m_bCtrlDown = false;
          break;
        }
        default:
          break;
      }
      return;
    }
    default:
      break;
  }
}

//-----------------------------------------------------------------------------

void visu_ActorPolygonPart::SetHiliData(const t_ptr<visu_DataSet>& data)
{
  m_hiliPart = t_ptr<visu_DataPolygonPart>::DownCast(data);
}

//-----------------------------------------------------------------------------

t_ptr<visu_DataSet> visu_ActorPolygonPart::GetHiliData() const
{
  return m_hiliPart;
}

//-----------------------------------------------------------------------------

void visu_ActorPolygonPart::ClearHiliData()
{
  if ( !m_hiliPart.IsNull() )
    m_hiliPart->Clear();
}

//-----------------------------------------------------------------------------

void visu_ActorPolygonPart::GL_Hili() const
{
  if ( m_hiliPart.IsNull() )
    return;

  // Outer loop.
  ::drawPolyline(m_hiliPart->GetPart()->GetOuterLoop()->GetPoles(), QrLightBlue, 2.f);

  // Inner loops.
  const std::vector< t_ptr<t_polygon> >& il = m_hiliPart->GetPart()->GetInnerLoops();
  //
  for ( const auto& loop : il )
  {
    ::drawPolyline(loop->GetPoles(), QrLightBlue, 2.f);
  }
}

//-----------------------------------------------------------------------------

void visu_ActorPolygonPart::SetColor(const visu_ColorRGB<GLubyte>& color)
{
  m_color = color;
}
