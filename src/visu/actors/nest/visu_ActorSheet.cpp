//-----------------------------------------------------------------------------
// Created on: 24 January 2025
//-----------------------------------------------------------------------------
// Copyright (c) 2025-present, Sergey Slyadnev
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
//    * Redistributions of source code must retain the above copyright
//      notice, this list of conditions and the following disclaimer.
//    * Redistributions in binary form must reproduce the above copyright
//      notice, this list of conditions and the following disclaimer in the
//      documentation and/or other materials provided with the distribution.
//    * Neither the name of Sergey Slyadnev nor the
//      names of all contributors may be used to endorse or promote products
//      derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------

// Windows
#include <windows.h>

// GL includes
#include <gl/gl.h>
#include <gl/glu.h>

// Own include
#include <mobius/visu_ActorSheet.h>

// visu includes
#include <mobius/visu_DataPositionCloud.h>

using namespace mobius;

//-----------------------------------------------------------------------------

visu_ActorSheet::visu_ActorSheet(const t_ptr<nest_Sheet>&      sheet,
                                 const visu_ColorRGB<GLubyte>& color)
: visu_Actor (),
  m_sheet    (sheet),
  m_color    (color)
{
}

//-----------------------------------------------------------------------------

visu_ActorSheet::~visu_ActorSheet()
{}

//-----------------------------------------------------------------------------

void visu_ActorSheet::GetBounds(double& xMin, double& xMax,
                                double& yMin, double& yMax,
                                double& zMin, double& zMax) const
{
  m_sheet->GetBounds(xMin, xMax, yMin, yMax);
  //
  zMin = 0.0;
  zMax = 0.0;
}

//-----------------------------------------------------------------------------

void visu_ActorSheet::GL_Draw()
{
  const t_ptr<SheetGrid>& grid = m_sheet->GetGrid();

  // Draw points.
  glPointSize(2);
  glBegin(GL_POINTS);
    //
    for ( int k = 0; k < grid->M; ++k )
    {
      const nest_GridPt& pt = grid->pArray[k];

      if ( pt.occupied )
      {
        glColor3ub(250, 60, 15);
      }
      else if ( pt.locked )
      {
        glColor3ub(255, 255, 0);
      }
      else
      {
        glColor3ub(m_color.R, m_color.G, m_color.B);
      }
     
      glVertex3f( (GLfloat) pt.P.U(), (GLfloat) pt.P.V(), (GLfloat) 0.f );
    }
    //
  glEnd();
}

//-----------------------------------------------------------------------------

t_ptr<visu_DataSet>
  visu_ActorSheet::IntersectWithLine(const t_ptr<geom_Line>& ray)
{
  const t_ptr<SheetGrid>& grid = m_sheet->GetGrid();

  // Data for highlighting.
  t_ptr<visu_DataPositionCloud> hiliData;

  // Working variables.
  double    best_prec = 5.0;
  int       idx       = -1;

  // Now intersect the entire cloud with ray.
  for ( int k = 0; k < grid->M; ++k )
  {
    const nest_GridPt& pt = grid->pArray[k];
    const t_xyz&       O  = ray->GetOrigin();
    t_xyz              P( pt.P.U(), pt.P.V(), 0. );

    t_xyz OP = P - O;
    const double ang = OP.Angle( ray->GetDir() );
    const double h = OP.Modulus() * sin(ang);

    if ( h < best_prec )
    {
      idx = (int) k;
      best_prec = h;
    }
  }

  // Fill data set with just one point.
  if ( idx != -1 )
  {
    hiliData = new visu_DataPositionCloud();
    hiliData->AddPoint( { grid->pArray[idx].P.U(),
                          grid->pArray[idx].P.V(),
                          0.0 } );

    int i = -1, j = -1;
    m_sheet->GetGrid()->FromPlain(idx, i, j);

    std::cout << "Picked point:\n";
    std::cout << "\tSerial (idx): " << idx                  << std::endl;
    std::cout << "\tPair (i,j):  (" << i << "," << j << ")" << std::endl;
  }

  return hiliData.Access();
}

//-----------------------------------------------------------------------------

void visu_ActorSheet::ProcessMessage(UINT   /*message*/,
                                     WPARAM /*wparam*/,
                                     LPARAM)
{
  // nothing to do
}

//-----------------------------------------------------------------------------

void visu_ActorSheet::SetHiliData(const t_ptr<visu_DataSet>& data)
{
  m_hiliCloud = t_ptr<visu_DataPositionCloud>::DownCast(data);
}

//-----------------------------------------------------------------------------

t_ptr<visu_DataSet> visu_ActorSheet::GetHiliData() const
{
  return m_hiliCloud;
}

//-----------------------------------------------------------------------------

void visu_ActorSheet::ClearHiliData()
{
  if ( !m_hiliCloud.IsNull() )
    m_hiliCloud->Clear();
}

//-----------------------------------------------------------------------------

void visu_ActorSheet::GL_Hili() const
{
  if ( m_hiliCloud.IsNull() )
    return;

  const t_ptr<t_pcloud>& pcl = m_hiliCloud->GetCloud();
  if ( pcl.IsNull() )
    return;

  glPointSize(10);
  glColor3ub(255, 0, 0);
  glBegin(GL_POINTS);
    //
    for ( int i = 0; i < pcl->GetNumberOfPoints(); ++i )
    {
      const t_xyz& P = pcl->GetPoint(i);
      glVertex3f( (GLfloat) P.X(), (GLfloat) P.Y(), (GLfloat) P.Z() );
    }
    //
  glEnd();
}
