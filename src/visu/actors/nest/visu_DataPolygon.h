//-----------------------------------------------------------------------------
// Created on: 20 May 2024
//-----------------------------------------------------------------------------
// Copyright (c) 2024-present, Sergey Slyadnev
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
//    * Redistributions of source code must retain the above copyright
//      notice, this list of conditions and the following disclaimer.
//    * Redistributions in binary form must reproduce the above copyright
//      notice, this list of conditions and the following disclaimer in the
//      documentation and/or other materials provided with the distribution.
//    * Neither the name of Sergey Slyadnev nor the
//      names of all contributors may be used to endorse or promote products
//      derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------

#ifndef visu_DataPolygon_HeaderFile
#define visu_DataPolygon_HeaderFile

// visu includes
#include <mobius/visu_DataSet.h>

// core includes
#include <mobius/core_Polygon.h>

namespace mobius {

//! \ingroup MOBIUS_VISU
//!
//! Visualization data set for a polygon.
class visu_DataPolygon : public visu_DataSet
{
public:

  mobiusVisu_EXPORT
    visu_DataPolygon(const t_ptr<t_polygon>& polygon = NULL);

  mobiusVisu_EXPORT virtual
    ~visu_DataPolygon();

public:

  //! Sets the polygon.
  //! \param[in] polygon the polygon to set for visualization.
  void SetPolygon(const t_ptr<t_polygon>& polygon)
  {
    m_polygon = polygon;
  }

  //! \return the stored polygon.
  const t_ptr<t_polygon>& GetPolygon() const
  {
    return m_polygon;
  }

public:

  mobiusVisu_EXPORT virtual void
    Join(const t_ptr<visu_DataSet>& data);

  mobiusVisu_EXPORT virtual void
    Clear();

private:

  //! Polygon to visualize.
  t_ptr<t_polygon> m_polygon;

};

}

#endif
