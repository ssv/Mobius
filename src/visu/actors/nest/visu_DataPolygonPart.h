//-----------------------------------------------------------------------------
// Created on: 24 January 2025
//-----------------------------------------------------------------------------
// Copyright (c) 2025-present, Sergey Slyadnev
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
//    * Redistributions of source code must retain the above copyright
//      notice, this list of conditions and the following disclaimer.
//    * Redistributions in binary form must reproduce the above copyright
//      notice, this list of conditions and the following disclaimer in the
//      documentation and/or other materials provided with the distribution.
//    * Neither the name of Sergey Slyadnev nor the
//      names of all contributors may be used to endorse or promote products
//      derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------

#ifndef visu_DataPolygonPart_HeaderFile
#define visu_DataPolygonPart_HeaderFile

// visu includes
#include <mobius/visu_DataSet.h>

// core includes
#include <mobius/core_PolygonPart.h>

namespace mobius {

//! \ingroup MOBIUS_VISU
//!
//! Visualization data set for a nestable part.
class visu_DataPolygonPart : public visu_DataSet
{
public:

  mobiusVisu_EXPORT
    visu_DataPolygonPart(const t_ptr<t_polygonPart>& part = nullptr);

  mobiusVisu_EXPORT virtual
    ~visu_DataPolygonPart();

public:

  //! Sets the part.
  //! \param[in] part the part to set for visualization.
  void SetPart(const t_ptr<t_polygonPart>& part)
  {
    m_part = part;
  }

  //! \return the stored part.
  const t_ptr<t_polygonPart>& GetPart() const
  {
    return m_part;
  }

public:

  mobiusVisu_EXPORT virtual void
    Join(const t_ptr<visu_DataSet>& data);

  mobiusVisu_EXPORT virtual void
    Clear();

private:

  //! Part to visualize.
  t_ptr<t_polygonPart> m_part;

};

}

#endif
