//-----------------------------------------------------------------------------
// Created on: 19 May 2024
//-----------------------------------------------------------------------------
// Copyright (c) 2024-present, Sergey Slyadnev
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
//    * Redistributions of source code must retain the above copyright
//      notice, this list of conditions and the following disclaimer.
//    * Redistributions in binary form must reproduce the above copyright
//      notice, this list of conditions and the following disclaimer in the
//      documentation and/or other materials provided with the distribution.
//    * Neither the name of Sergey Slyadnev nor the
//      names of all contributors may be used to endorse or promote products
//      derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------

#ifndef visu_AddPolygonPoleCmd_HeaderFile
#define visu_AddPolygonPoleCmd_HeaderFile

// visu includes
#include <mobius/visu_ActorPolygon.h>
#include <mobius/visu_ViewCmd.h>

// core includes
#include <mobius/core_Polygon.h>

namespace mobius {

//! \ingroup MOBIUS_VISU
//!
//! Adds another pole to a 2D nestable polygon.
class visu_AddPolygonPoleCmd : public visu_ViewCmd
{
public:

  //! Constructor.
  //! \param[in] CmdRepo command repo.
  //! \param[in] Picker  instance of Picker.
  visu_AddPolygonPoleCmd(const t_ptr<visu_CommandRepo>& CmdRepo,
                         const t_ptr<visu_Picker>&      Picker)
  : visu_ViewCmd(CmdRepo, Picker) {}

  //! Destructor.
  virtual ~visu_AddPolygonPoleCmd() {}

public:

  //! Returns human-readable name of the command.
  //! \return name.
  inline virtual std::string Name() const
  {
    return "Add polygon pole";
  }

public:

  //! Executes command.
  //! \return true in case of success, false -- otherwise.
  virtual bool Execute()
  {
    std::cout << this->Name() << std::endl;
    if ( this->Argc() < 2 )
    {
      std::cout << "Error: polygon name expected" << std::endl;
      return false;
    }

    // Get Qr variable
    std::string name = this->Arg<std::string>(0, "#error");

    // Find target polygon in the global context
    t_ptr<core_Polygon>
      polygon = t_ptr<core_Polygon>::DownCast( this->CmdRepo()->Vars().FindVariable(name) );
    //
    if ( polygon.IsNull() )
    {
      std::cout << "polygon is expected" << std::endl;
      return false;
    }

    /* ================
     *  Modify polygon.
     * ================ */

    double x = this->Arg<double>(1, 0.);
    double y = this->Arg<double>(2, 0.);
    //
    polygon->AddPole( {x, y} );

    /* ==============
     *  Adjust scene.
     * ============== */

    // Rebind actor
    t_ptr<visu_ActorPolygon>
      polygonActor = new visu_ActorPolygon(polygon, QrRed);
    //
    this->Scene()->Add(polygonActor.Access(), name);

    // Rebind variable
    this->CmdRepo()->Vars().RegisterVariable( name, polygon.Access() );

    return true;
  }

};

}

#endif
