//-----------------------------------------------------------------------------
// Created on: 30 January 2025
//-----------------------------------------------------------------------------
// Copyright (c) 2025-present, Sergey Slyadnev
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
//    * Redistributions of source code must retain the above copyright
//      notice, this list of conditions and the following disclaimer.
//    * Redistributions in binary form must reproduce the above copyright
//      notice, this list of conditions and the following disclaimer in the
//      documentation and/or other materials provided with the distribution.
//    * Neither the name of Sergey Slyadnev nor the
//      names of all contributors may be used to endorse or promote products
//      derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------

#ifndef visu_BuildPatternCmd_HeaderFile
#define visu_BuildPatternCmd_HeaderFile

// visu includes
#include <mobius/visu_ActorPolygonPart.h>
#include <mobius/visu_ViewCmd.h>

// nest includes
#include <mobius/nest_Part.h>
#include <mobius/nest_Sheet.h>

namespace mobius {

//! \ingroup MOBIUS_VISU
//!
//! (Re)builds a grid pattern for a nestable part.
class visu_BuildPatternCmd : public visu_ViewCmd
{
public:

  //! Constructor.
  //! \param[in] CmdRepo command repo.
  //! \param[in] Picker  instance of Picker.
  visu_BuildPatternCmd(const t_ptr<visu_CommandRepo>& CmdRepo,
                       const t_ptr<visu_Picker>&      Picker)
  : visu_ViewCmd(CmdRepo, Picker) {}

  //! Destructor.
  virtual ~visu_BuildPatternCmd() {}

public:

  //! Returns human-readable name of the command.
  //! \return name.
  inline virtual std::string Name() const
  {
    return "Build pattern";
  }

public:

  //! Executes command.
  //! \return true in case of success, false -- otherwise.
  virtual bool Execute()
  {
    std::cout << this->Name() << std::endl;
    if ( this->Argc() != 1 )
    {
      std::cout << "Error: part name is expected." << std::endl;
      return false;
    }

    // Get variable.
    std::string name = this->Arg<std::string>(0, "#error");

    // Find target part in the global context.
    t_ptr<nest_Part>
      part = t_ptr<nest_Part>::DownCast( this->CmdRepo()->Vars().FindVariable(name) );
    //
    if ( part.IsNull() )
    {
      std::cout << "Error: nestable part is expected." << std::endl;
      return false;
    }

    /* =============
     *  Update part.
     * ============= */

    if ( part->HasContainer() )
    {
      t_ptr<nest_Sheet>
        sheet = t_ptr<nest_Sheet>::DownCast( part->GetContainer() );

      sheet->BuildPointPattern(part);
    }
    else
    {
      std::cout << "Error: this nestable part does not have a grid." << std::endl;
      return false;
    }

    /* ==============
     *  Adjust scene.
     * ============== */

    // Rebind actor.
    t_ptr<visu_ActorPolygonPart>
      actor = new visu_ActorPolygonPart(part, QrRed);
    //
    this->Scene()->Add(actor.Access(), name);

    // Rebind variable.
    this->CmdRepo()->Vars().RegisterVariable( name, part.Access() );

    return true;
  }

};

}

#endif
