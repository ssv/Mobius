//-----------------------------------------------------------------------------
// Created on: 24 January 2025
//-----------------------------------------------------------------------------
// Copyright (c) 2025-present, Sergey Slyadnev
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
//    * Redistributions of source code must retain the above copyright
//      notice, this list of conditions and the following disclaimer.
//    * Redistributions in binary form must reproduce the above copyright
//      notice, this list of conditions and the following disclaimer in the
//      documentation and/or other materials provided with the distribution.
//    * Neither the name of Sergey Slyadnev nor the
//      names of all contributors may be used to endorse or promote products
//      derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------

#ifndef visu_ImportPartCmd_HeaderFile
#define visu_ImportPartCmd_HeaderFile

// visu includes
#include <mobius/visu_ActorPolygonPart.h>
#include <mobius/visu_ViewCmd.h>

// nest includes
#include <mobius/nest_Part.h>

namespace mobius {

//! \ingroup MOBIUS_VISU
//!
//! Imports a 2D nestable part from file and visualizes it.
class visu_ImportPartCmd : public visu_ViewCmd
{
public:

  //! Constructor.
  //! \param[in] CmdRepo command repo.
  //! \param[in] Picker  instance of Picker.
  visu_ImportPartCmd(const t_ptr<visu_CommandRepo>& CmdRepo,
                     const t_ptr<visu_Picker>&      Picker)
  : visu_ViewCmd(CmdRepo, Picker) {}

  //! Destructor.
  virtual ~visu_ImportPartCmd() {}

public:

  //! Returns human-readable name of the command.
  //! \return name.
  inline virtual std::string Name() const
  {
    return "Import nestable part";
  }

public:

  //! Executes command.
  //! \return true in case of success, false -- otherwise.
  virtual bool Execute()
  {
    std::cout << this->Name() << std::endl;

    const size_t argc = this->Argc();

    if ( argc != 1 )
    {
      std::cout << "Error: filename is expected" << std::endl;
      return false;
    }

    // Get arguments.
    std::string filename = this->Arg<std::string>(0, "#error");

    /* =============
     *  Create part.
     * ============= */

    t_ptr<nest_Part> part = nest_Part::Import(filename);
    //
    if ( part.IsNull() )
    {
      std::cout << "Error: failed to import a part from file '"
                << filename << "'."
                << std::endl;
      //
      return false;
    }

    /* ==============
     *  Adjust scene.
     * ============== */

    // Create actor.
    t_ptr<visu_ActorPolygonPart>
      partActor = new visu_ActorPolygonPart(part, QrRed);

    // Bind variable.
    this->CmdRepo()->Vars().RegisterVariable( part->GetName(), part.Access() );

    this->Scene()->Add( partActor.Access(), part->GetName() );

    return true;
  }

};

}

#endif
