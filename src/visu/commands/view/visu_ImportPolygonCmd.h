//-----------------------------------------------------------------------------
// Created on: 26 June 2024
//-----------------------------------------------------------------------------
// Copyright (c) 2024-present, Sergey Slyadnev
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
//    * Redistributions of source code must retain the above copyright
//      notice, this list of conditions and the following disclaimer.
//    * Redistributions in binary form must reproduce the above copyright
//      notice, this list of conditions and the following disclaimer in the
//      documentation and/or other materials provided with the distribution.
//    * Neither the name of Sergey Slyadnev nor the
//      names of all contributors may be used to endorse or promote products
//      derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------

#ifndef visu_ImportPolygonCmd_HeaderFile
#define visu_ImportPolygonCmd_HeaderFile

// visu includes
#include <mobius/visu_ActorPolygon.h>
#include <mobius/visu_ViewCmd.h>

// core includes
#include <mobius/core_Polygon.h>

namespace mobius {

//! \ingroup MOBIUS_VISU
//!
//! Imports a 2D nestable polygon from file and visualizes it.
class visu_ImportPolygonCmd : public visu_ViewCmd
{
public:

  //! Constructor.
  //! \param[in] CmdRepo command repo.
  //! \param[in] Picker  instance of Picker.
  visu_ImportPolygonCmd(const t_ptr<visu_CommandRepo>& CmdRepo,
                        const t_ptr<visu_Picker>&      Picker)
  : visu_ViewCmd(CmdRepo, Picker) {}

  //! Destructor.
  virtual ~visu_ImportPolygonCmd() {}

public:

  //! Returns human-readable name of the command.
  //! \return name.
  inline virtual std::string Name() const
  {
    return "Import nestable polygon";
  }

public:

  //! Executes command.
  //! \return true in case of success, false -- otherwise.
  virtual bool Execute()
  {
    std::cout << this->Name() << std::endl;

    const size_t argc = this->Argc();

    if ( argc < 2 )
    {
      std::cout << "Error: filename and polygon name are expected" << std::endl;
      return false;
    }

    // Get arguments.
    std::string filename = this->Arg<std::string>(0, "#error");
    std::string name     = this->Arg<std::string>(1, "#error");

    /* ================
     *  Create polygon.
     * ================ */

    t_ptr<core_Polygon> polygon = core_Polygon::Import(filename);
    //
    if ( polygon.IsNull() )
    {
      std::cout << "Error: failed to import polygon from file '"
                << filename << "'"
                << std::endl;
      //
      return false;
    }

    polygon->SetName(name);

    /* ==============
     *  Adjust scene
     * ============== */

    // Create actor.
    t_ptr<visu_ActorPolygon> polygonActor = new visu_ActorPolygon(polygon, QrRed);

    // Bind variable.
    this->CmdRepo()->Vars().RegisterVariable( name, polygon.Access() );

    this->Scene()->Add( polygonActor.Access(), name );
    this->Scene()->InstallAxes();

    return true;
  }

};

}

#endif
