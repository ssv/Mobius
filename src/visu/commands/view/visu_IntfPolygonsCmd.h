//-----------------------------------------------------------------------------
// Created on: 09 June 2024
//-----------------------------------------------------------------------------
// Copyright (c) 2024-present, Sergey Slyadnev
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
//    * Redistributions of source code must retain the above copyright
//      notice, this list of conditions and the following disclaimer.
//    * Redistributions in binary form must reproduce the above copyright
//      notice, this list of conditions and the following disclaimer in the
//      documentation and/or other materials provided with the distribution.
//    * Neither the name of Sergey Slyadnev nor the
//      names of all contributors may be used to endorse or promote products
//      derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------

#ifndef visu_IntfPolygonsCmd_HeaderFile
#define visu_IntfPolygonsCmd_HeaderFile

// visu includes
#include <mobius/visu_ActorPolygon.h>
#include <mobius/visu_ActorPositionCloud.h>
#include <mobius/visu_ViewCmd.h>

// core includes
#include <mobius/core_Polygon.h>

namespace mobius {

//! \ingroup MOBIUS_VISU
//!
//! Intersects a couple of 2D nestable polygons.
class visu_IntfPolygonsCmd : public visu_ViewCmd
{
public:

  //! Constructor.
  //! \param[in] CmdRepo command repo.
  //! \param[in] Picker  instance of Picker.
  visu_IntfPolygonsCmd(const t_ptr<visu_CommandRepo>& CmdRepo,
                       const t_ptr<visu_Picker>&      Picker)
  : visu_ViewCmd(CmdRepo, Picker) {}

  //! Destructor.
  virtual ~visu_IntfPolygonsCmd() {}

public:

  //! Returns human-readable name of the command.
  //! \return name.
  inline virtual std::string Name() const
  {
    return "Intersect nestable polygons";
  }

public:

  //! Executes command.
  //! \return true in case of success, false -- otherwise.
  virtual bool Execute()
  {
    std::cout << this->Name() << std::endl;

    const size_t argc = this->Argc();

    if ( argc < 2 )
    {
      std::cout << "Error: polygon names are expected" << std::endl;
      return false;
    }

    // Get polygon names.
    std::string name1 = this->Arg<std::string>(0, "#error");
    std::string name2 = this->Arg<std::string>(1, "#error");

    // Find target polygons in the global context.
    t_ptr<core_Polygon>
      polygon1 = t_ptr<core_Polygon>::DownCast( this->CmdRepo()->Vars().FindVariable(name1) );
    //
    t_ptr<core_Polygon>
      polygon2 = t_ptr<core_Polygon>::DownCast( this->CmdRepo()->Vars().FindVariable(name2) );
    //
    if ( polygon1.IsNull() || polygon2.IsNull() )
    {
      std::cout << "Error: two polygons are expected" << std::endl;
      return false;
    }

    /* ====================
     *  Intersect polygons.
     * ==================== */

    std::vector<t_uv> ipts;
    //
    if ( polygon1->Intersects(polygon2, ipts) )
    {
      std::cout << "polygons have " << ipts.size() << " intersection point(s)" << std::endl;
    }
    else
    {
      if ( polygon1->IsInsideOf(polygon2) )
      {
        std::cout << polygon1->GetName() << " is inside " << polygon2->GetName() << std::endl;
      }
      else if ( polygon2->IsInsideOf(polygon1) )
      {
        std::cout << polygon2->GetName() << " is inside " << polygon1->GetName() << std::endl;
      }
      else
      {
        std::cout << "polygons do not intersect" << std::endl;
      }
    }

    // Collect intersection points for visualization
    t_ptr<t_pcloud> pcloud = new t_pcloud;
    //
    for ( const auto& ip : ipts )
    {
      pcloud->AddPoint( t_xyz(ip.U(), ip.V(), 0.) );
    }

    /* ==============
     *  Adjust scene
     * ============== */

    // Rebind actor
    t_ptr<visu_ActorPositionCloud>
      ptsActor = new visu_ActorPositionCloud(pcloud, QrGreen);
    //
    ptsActor->SetPointSize(14.f);
    ptsActor->SetSelectable(false);
    //
    this->Scene()->Add(ptsActor.Access(), "ipts");

    return true;
  }

};

}

#endif
