//-----------------------------------------------------------------------------
// Created on: 15 May 2024
//-----------------------------------------------------------------------------
// Copyright (c) 2024-present, Sergey Slyadnev
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
//    * Redistributions of source code must retain the above copyright
//      notice, this list of conditions and the following disclaimer.
//    * Redistributions in binary form must reproduce the above copyright
//      notice, this list of conditions and the following disclaimer in the
//      documentation and/or other materials provided with the distribution.
//    * Neither the name of Sergey Slyadnev nor the
//      names of all contributors may be used to endorse or promote products
//      derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------

#ifndef visu_MakePolygonCmd_HeaderFile
#define visu_MakePolygonCmd_HeaderFile

// visu includes
#include <mobius/visu_ActorPolygon.h>
#include <mobius/visu_ViewCmd.h>

// core includes
#include <mobius/core_Polygon.h>

namespace mobius {

//! \ingroup MOBIUS_VISU
//!
//! Creates and visualizes a 2D nestable polygon.
class visu_MakePolygonCmd : public visu_ViewCmd
{
public:

  //! Constructor.
  //! \param[in] CmdRepo command repo.
  //! \param[in] Picker  instance of Picker.
  visu_MakePolygonCmd(const t_ptr<visu_CommandRepo>& CmdRepo,
                      const t_ptr<visu_Picker>&      Picker)
  : visu_ViewCmd(CmdRepo, Picker) {}

  //! Destructor.
  virtual ~visu_MakePolygonCmd() {}

public:

  //! Returns human-readable name of the command.
  //! \return name.
  inline virtual std::string Name() const
  {
    return "Make nestable polygon";
  }

public:

  //! Executes command.
  //! \return true in case of success, false -- otherwise.
  virtual bool Execute()
  {
    std::cout << this->Name() << std::endl;

    const size_t argc = this->Argc();

    if ( argc < 1 )
    {
      std::cout << "Error: polygon name expected" << std::endl;
      return false;
    }

    // Get Qr variable
    std::string name = this->Arg<std::string>(0, "#error");

    /* ================
     *  Create polygon.
     * ================ */

    t_ptr<core_Polygon> polygon = new core_Polygon;
    //
    polygon->SetName(name);

    // Add poles.
    if ( argc > 1 )
    {
      for ( size_t i = 1; i < argc - 1; i += 2 )
      {
        double u = this->Arg<double>(i, 0.);
        double v = this->Arg<double>(i + 1, 0.);

        polygon->AddPole( {u, v} );
      }
      //
      if ( this->HasKeyword("close") )
        polygon->Close();
    }

    /* ==============
     *  Adjust scene
     * ============== */

    // Create actor.
    t_ptr<visu_ActorPolygon> polygonActor = new visu_ActorPolygon(polygon, QrRed);

    // Bind variable.
    this->CmdRepo()->Vars().RegisterVariable( name, polygon.Access() );

    this->Scene()->Add( polygonActor.Access(), name );
    this->Scene()->InstallAxes();

    return true;
  }

};

}

#endif
