//-----------------------------------------------------------------------------
// Created on: 28 January 2025
//-----------------------------------------------------------------------------
// Copyright (c) 2025-present, Sergey Slyadnev
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
//    * Redistributions of source code must retain the above copyright
//      notice, this list of conditions and the following disclaimer.
//    * Redistributions in binary form must reproduce the above copyright
//      notice, this list of conditions and the following disclaimer in the
//      documentation and/or other materials provided with the distribution.
//    * Neither the name of Sergey Slyadnev nor the
//      names of all contributors may be used to endorse or promote products
//      derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------

#ifndef visu_NestCmd_HeaderFile
#define visu_NestCmd_HeaderFile

// visu includes
#include <mobius/visu_ActorPolygonPart.h>
#include <mobius/visu_IV.h>
#include <mobius/visu_ProgressNotifier.h>
#include <mobius/visu_ViewCmd.h>

// nest includes
#include <mobius/nest_BLF.h>

namespace mobius {

//! \ingroup MOBIUS_VISU
//!
//! Nests a 2D nestable part in the provided sheet.
class visu_NestCmd : public visu_ViewCmd
{
public:

  //! Constructor.
  //! \param[in] CmdRepo command repo.
  //! \param[in] Picker  instance of Picker.
  visu_NestCmd(const t_ptr<visu_CommandRepo>& CmdRepo,
               const t_ptr<visu_Picker>&      Picker)
  : visu_ViewCmd(CmdRepo, Picker) {}

  //! Destructor.
  virtual ~visu_NestCmd() {}

public:

  //! Returns human-readable name of the command.
  //! \return name.
  inline virtual std::string Name() const
  {
    return "Nests a part";
  }

public:

  //! Executes command.
  //! \return true in case of success, false -- otherwise.
  virtual bool Execute()
  {
    std::cout << this->Name() << std::endl;

    /* ===================
     *  Prepare arguments.
     * =================== */

    const size_t argc = this->Argc();

    if ( argc != 2 )
    {
      std::cout << "Error: filename and sheet name are expected." << std::endl;
      return false;
    }

    // Get arguments.
    std::string filename  = this->Arg<std::string>(0, "#error");
    std::string sheetname = this->Arg<std::string>(1, "#error");

    // Access the sheet.
    t_ptr<nest_Sheet>
      sheet = t_ptr<nest_Sheet>::DownCast( this->CmdRepo()->Vars().FindVariable(sheetname) );
    //
    if ( sheet.IsNull() )
    {
      std::cout << "Error: there is no sheet with the name '"
                << sheetname << "'." << std::endl;
      return false;
    }

    // Import a part.
    t_ptr<nest_Part> part = nest_Part::Import(filename);
    //
    if ( part.IsNull() )
    {
      std::cout << "Error: failed to import a part from file '"
                << filename << "'."
                << std::endl;
      //
      return false;
    }

    /* =================
     *  Perform nesting.
     * ================= */

    // Prepare diagnostic tools.
    core_ProgressEntry progress ( new visu_ProgressNotifier(std::cout) );
    geom_PlotterEntry  plotter  ( new visu_IV( this->Scene(), this->CmdRepo() ) );

    // Prepare the algorithm.
    t_ptr<nest_BLF> blf = new nest_BLF(sheet, progress, plotter);

    // Nest.
    double           elapsedTime   = 0;
    int              numCollisions = 0;
    t_ptr<nest_Part> nested;
    //
    if ( !blf->PlacePart(part, nested, elapsedTime, numCollisions) )
    {
      std::cout << "Error: failed to nest the part '" << part->GetName()
                << "' to the sheet '" << sheetname << "'."
                << std::endl;
      //
      return false;
    }

    std::cout << "The part '" << part->GetName()
              << "' has been nested to the sheet '" << sheetname << "'."
              << std::endl;

    if ( !nested.IsNull() )
    {
      // Set a unique name.
      const int numNested = (int) ( sheet->GetNested().size() );
      //
      nested->SetName( "nested_" + core::str::to_string(numNested) );

      // Create actor.
      t_ptr<visu_ActorPolygonPart>
        nestedActor = new visu_ActorPolygonPart(nested, QrGreenish);

      // Bind variable.
      this->CmdRepo()->Vars().RegisterVariable( nested->GetName(), nested.Access() );

      this->Scene()->Add( nestedActor.Access(), nested->GetName() );
    }

    return true;
  }

};

}

#endif
