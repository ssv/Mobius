//-----------------------------------------------------------------------------
// Created on: 21 January 2014
//-----------------------------------------------------------------------------
// Copyright (c) 2013-present, Sergey Slyadnev
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
//    * Redistributions of source code must retain the above copyright
//      notice, this list of conditions and the following disclaimer.
//    * Redistributions in binary form must reproduce the above copyright
//      notice, this list of conditions and the following disclaimer in the
//      documentation and/or other materials provided with the distribution.
//    * Neither the name of Sergey Slyadnev nor the
//      names of all contributors may be used to endorse or promote products
//      derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------

#ifndef visu_ColorRGB_HeaderFile
#define visu_ColorRGB_HeaderFile

// visu includes
#include <mobius/visu.h>

namespace mobius {

//! \ingroup MOBIUS_VISU
//!
//! Simple structure representing color in RGB format.
template<typename T>
struct visu_ColorRGB
{
  T R; //!< Red.
  T G; //!< Green.
  T B; //!< Blue.

  //! Default constructor.
  visu_ColorRGB() : R(0), G(0), B(0) {}

  //! Complete constructor.
  //! \param r [in] red component.
  //! \param g [in] green component.
  //! \param b [in] blue component.
  visu_ColorRGB(const T r, const T g, const T b) : R(r), G(g), B(b) {}
};

}

//-----------------------------------------------------------------------------
// Useful macro
//-----------------------------------------------------------------------------

#define QrBlack     visu_ColorRGB<GLubyte>(0,     0,   0)
#define QrWhite     visu_ColorRGB<GLubyte>(255, 255, 255)
#define QrRed       visu_ColorRGB<GLubyte>(255,   0,   0)
#define QrReddish   visu_ColorRGB<GLubyte>(250,  60,  15)
#define QrGreen     visu_ColorRGB<GLubyte>(0,   255,   0)
#define QrGreenish  visu_ColorRGB<GLubyte>(140, 220,  40)
#define QrBlue      visu_ColorRGB<GLubyte>(0,     0, 255)
#define QrBlueish   visu_ColorRGB<GLubyte>(50,  150, 255)
#define QrLightBlue visu_ColorRGB<GLubyte>(0,   145, 230)
#define QrYellow    visu_ColorRGB<GLubyte>(255, 255,   0)
#define QrGray      visu_ColorRGB<GLubyte>(25,   25,  25)

#endif
