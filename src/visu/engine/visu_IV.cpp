//-----------------------------------------------------------------------------
// Created on: 28 January 2025
//-----------------------------------------------------------------------------
// Copyright (c) 2025-present, Sergey Slyadnev
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
//    * Redistributions of source code must retain the above copyright
//      notice, this list of conditions and the following disclaimer.
//    * Redistributions in binary form must reproduce the above copyright
//      notice, this list of conditions and the following disclaimer in the
//      documentation and/or other materials provided with the distribution.
//    * Neither the name of Sergey Slyadnev nor the
//      names of all contributors may be used to endorse or promote products
//      derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------

// Windows-aware includes
#include <mobius/visu_ViewWindow.h>

// Own include
#include <mobius/visu_IV.h>

// Visu includes
#include <mobius/visu_ActorPolygon.h>
#include <mobius/visu_ActorPolygonPart.h>
#include <mobius/visu_ActorPositionCloud.h>
#include <mobius/visu_ColorSelector.h>

// Geom includes
#include <mobius/geom_PositionCloud.h>

using namespace mobius;

#define GLDRAW /*\
  this->Scene()->ViewWindow()->Draw(); \
  this->Scene()->ViewWindow()->SwapBuff();*/

//-----------------------------------------------------------------------------

void visu_IV::ERASE(const std::string& name)
{
  this->Scene()->Clear(name);

  GLDRAW
}

//-----------------------------------------------------------------------------

void visu_IV::DRAW_POINTS(const std::vector<t_uv>& pts,
                          const core_Color&        color,
                          const std::string&       name)
{
  // Find or create the actor.
  t_ptr<visu_Actor> actor = this->Scene()->FindActor(name);
  //
  if ( actor.IsNull() )
  {
    // Build cloud.
    t_ptr<t_pcloud> cloud = new t_pcloud();
    //
    for ( const auto& pt : pts )
      cloud->AddPoint(pt);

    // Prepare actor.
    actor = new visu_ActorPositionCloud( cloud, visu_ColorSelector::Color(color) );
    //
    t_ptr<visu_ActorPositionCloud>::DownCast(actor)->SetPointSize( (GLfloat) 5.f );

    // Populate scene.
    this->Scene()->Add( actor.Access(), name );

    // Bind variable.
    this->CmdRepo()->Vars().RegisterVariable(name, cloud);
  }
  else
  {
    actor->GL_Draw();
  }

  GLDRAW
}

//-----------------------------------------------------------------------------

void visu_IV::REDRAW_POINTS(const std::string&       name,
                            const std::vector<t_uv>& pts,
                            const core_Color&        color)
{
  this->DRAW_POINTS(pts, color, name);
}

//-----------------------------------------------------------------------------

void visu_IV::DRAW_POINTS(const std::vector<t_xyz>& pts,
                          const core_Color&         color,
                          const std::string&        name)
{
  // Find or create the actor.
  t_ptr<visu_Actor> actor = this->Scene()->FindActor(name);
  //
  if ( actor.IsNull() )
  {
    // Build cloud.
    t_ptr<t_pcloud> cloud = new t_pcloud();
    //
    for ( const auto& pt : pts )
      cloud->AddPoint(pt);

    // Prepare actor.
    actor = new visu_ActorPositionCloud( cloud, visu_ColorSelector::Color(color) );
    //
    t_ptr<visu_ActorPositionCloud>::DownCast(actor)->SetPointSize( (GLfloat) 5.f );

    // Populate scene.
    this->Scene()->Add( actor.Access(), name );

    // Bind variable.
    this->CmdRepo()->Vars().RegisterVariable(name, cloud);
  }
  else
  {
    actor->GL_Draw();
  }

  GLDRAW
}

//-----------------------------------------------------------------------------

void visu_IV::REDRAW_POINTS(const std::string&        name,
                            const std::vector<t_xyz>& pts,
                            const core_Color&         color)
{
  this->DRAW_POINTS(pts, color, name);
}

//-----------------------------------------------------------------------------

void visu_IV::DRAW_POLYGON(const t_ptr<core_Polygon>& polygon,
                           const core_Color&          color,
                           const std::string&         name)
{
  // Find or create the actor.
  t_ptr<visu_Actor> actor = this->Scene()->FindActor(name);
  //
  if ( actor.IsNull() )
  {
    // Create actor.
    actor = new visu_ActorPolygon( polygon, visu_ColorSelector::Color(color) );

    this->Scene()->Add( actor.Access(), name );

    // Bind variable.
    this->CmdRepo()->Vars().RegisterVariable(name, polygon);
  }
  else
  {
    actor->GL_Draw();
  }

  GLDRAW
}

//-----------------------------------------------------------------------------

void visu_IV::REDRAW_POLYGON(const std::string&         name,
                             const t_ptr<core_Polygon>& polygon,
                             const core_Color&          color)
{
  this->DRAW_POLYGON(polygon, color, name);
}

//-----------------------------------------------------------------------------

void visu_IV::DRAW_POLYGON_PART(const t_ptr<core_PolygonPart>& part,
                                const core_Color&              color,
                                const std::string&             name)
{
  // Find or create the actor.
  t_ptr<visu_Actor> actor = this->Scene()->FindActor(name);
  //
  if ( actor.IsNull() )
  {
    // Create actor.
    actor = new visu_ActorPolygonPart( part, visu_ColorSelector::Color(color) );

    // Add actor to the scene.
    this->Scene()->Add( actor.Access(), name );

    // Bind variable.
    this->CmdRepo()->Vars().RegisterVariable(name, part);
  }
  else
  {
    t_ptr<visu_ActorPolygonPart>::DownCast(actor)->Init(part);

    actor->SetColor( visu_ColorSelector::Color(color) );

    actor->GL_Draw();
  }

  GLDRAW
}

//-----------------------------------------------------------------------------

void visu_IV::REDRAW_POLYGON_PART(const std::string&             name,
                                  const t_ptr<core_PolygonPart>& part,
                                  const core_Color&              color)
{
  this->DRAW_POLYGON_PART(part, color, name);
}

//-----------------------------------------------------------------------------

void visu_IV::DRAW_AXES(const core_UV&     origin,
                        const core_UV&     dU,
                        const core_UV&     dV,
                        const core_Color&  color,
                        const std::string& name)
{
  t_ptr<t_polygon>
    pgon = new t_polygon({origin + dU, origin, origin + dV});

  // Find or create the actor.
  t_ptr<visu_Actor> actor = this->Scene()->FindActor(name);
  //
  if ( actor.IsNull() )
  {
    // Create actor.
    actor = new visu_ActorPolygon( pgon, visu_ColorSelector::Color(color) );

    this->Scene()->Add( actor.Access(), name );

    // Bind variable.
    this->CmdRepo()->Vars().RegisterVariable(name, pgon);
  }
  else
  {
    t_ptr<visu_ActorPolygon>::DownCast(actor)->Init(pgon);

    actor->SetColor( visu_ColorSelector::Color(color) );

    actor->GL_Draw();
  }

  GLDRAW
}

//-----------------------------------------------------------------------------

void visu_IV::REDRAW_AXES(const std::string& name,
                          const core_UV&     origin,
                          const core_UV&     dU,
                          const core_UV&     dV,
                          const core_Color&  color)
{
  this->DRAW_AXES(origin, dU, dV, color, name);
}
