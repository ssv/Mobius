//-----------------------------------------------------------------------------
// Created on: 28 January 2025
//-----------------------------------------------------------------------------
// Copyright (c) 2025-present, Sergey Slyadnev
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
//    * Redistributions of source code must retain the above copyright
//      notice, this list of conditions and the following disclaimer.
//    * Redistributions in binary form must reproduce the above copyright
//      notice, this list of conditions and the following disclaimer in the
//      documentation and/or other materials provided with the distribution.
//    * Neither the name of Sergey Slyadnev nor the
//      names of all contributors may be used to endorse or promote products
//      derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------

#ifndef visu_IV_h
#define visu_IV_h

// Visu includes
#include <mobius/visu_BaseCmd.h>
#include <mobius/visu_CommandRepo.h>
#include <mobius/visu_Scene.h>

// Geom includes
#include <mobius/geom_IPlotter.h>

//-----------------------------------------------------------------------------

namespace mobius {

//! \ingroup MOBIUS_VISU
//!
//! Interface for Imperative Viewer. A particular algorithm may benefit
//! from immediate plotting of its geometric variables in a unified way
//! thanks to this tool.
class visu_IV : public geom_IPlotter
{
public:

  //! Ctor.
  visu_IV(const t_ptr<visu_Scene>&       scene,
          const t_ptr<visu_CommandRepo>& cmd)
  //
  : geom_IPlotter (),
    m_scene       (scene),
    m_cmd         (cmd)
  {}

// COMMON:
public:

  virtual void
    ERASE_ALL() {}

  mobiusVisu_EXPORT virtual void
    ERASE(const std::string&);

// GEOMETRY:
public:

  //-------------------------------------------------------------------------//

  virtual void
    DRAW_POINT(const core_UV&,
               const core_Color&,
               const std::string&) {}

  virtual void
    DRAW_POINT(const core_XYZ&,
               const core_Color&,
               const std::string&) {}

  virtual void
    REDRAW_POINT(const std::string&,
                 const core_UV&,
                 const core_Color&) {}

  virtual void
    REDRAW_POINT(const std::string&,
                 const core_XYZ&,
                 const core_Color&) {}

  //-------------------------------------------------------------------------//

  mobiusVisu_EXPORT virtual void
    DRAW_POINTS(const std::vector<core_UV>&,
                const core_Color&,
                const std::string&);

  mobiusVisu_EXPORT virtual void
    REDRAW_POINTS(const std::string&,
                  const std::vector<core_UV>&,
                  const core_Color&);

  mobiusVisu_EXPORT virtual void
    DRAW_POINTS(const std::vector<core_XYZ>&,
                const core_Color&,
                const std::string&);

  mobiusVisu_EXPORT virtual void
    REDRAW_POINTS(const std::string&,
                  const std::vector<core_XYZ>&,
                  const core_Color&);

  //-------------------------------------------------------------------------//

  mobiusVisu_EXPORT virtual void
    DRAW_POLYGON(const t_ptr<core_Polygon>&,
                 const core_Color&,
                 const std::string&);

  mobiusVisu_EXPORT virtual void
    REDRAW_POLYGON(const std::string&,
                   const t_ptr<core_Polygon>&,
                   const core_Color&);
  //-------------------------------------------------------------------------//

  mobiusVisu_EXPORT virtual void
    DRAW_POLYGON_PART(const t_ptr<core_PolygonPart>&,
                      const core_Color&,
                      const std::string&);

  mobiusVisu_EXPORT virtual void
    REDRAW_POLYGON_PART(const std::string&,
                        const t_ptr<core_PolygonPart>&,
                        const core_Color&);

  //-------------------------------------------------------------------------//

  virtual void
    DRAW_VECTOR_AT(const core_XYZ&,
                   const core_XYZ&,
                   const core_Color&,
                   const std::string&) {}

  virtual void
    REDRAW_VECTOR_AT(const std::string&,
                     const core_XYZ&,
                     const core_XYZ&,
                     const core_Color&) {}

  //-------------------------------------------------------------------------//

  virtual void
    DRAW_CURVE(const t_ptr<geom_Curve>&,
               const core_Color&,
               const std::string&) {}

  virtual void
    REDRAW_CURVE(const std::string&,
                 const t_ptr<geom_Curve>&,
                 const core_Color&) {}

  //-------------------------------------------------------------------------//

  virtual void
    DRAW_SURFACE(const t_ptr<geom_Surface>&,
                 const core_Color&,
                 const std::string&) {}

  virtual void
    DRAW_SURFACE(const t_ptr<geom_Surface>&,
                 const core_Color&,
                 const double, // opacity
                 const std::string&) {}

  virtual void
    DRAW_SURFACE(const t_ptr<geom_Surface>&,
                 const double, // U min
                 const double, // U max
                 const double, // V min
                 const double, // V max
                 const core_Color&,
                 const std::string&) {}

  virtual void
    DRAW_SURFACE(const t_ptr<geom_Surface>&,
                 const double, // U min
                 const double, // U max
                 const double, // V min
                 const double, // V max
                 const core_Color&,
                 const double, // opacity
                 const std::string&) {}

  virtual void
    REDRAW_SURFACE(const std::string&,
                   const t_ptr<geom_Surface>&,
                   const core_Color&) {}

  virtual void
    REDRAW_SURFACE(const std::string&,
                   const t_ptr<geom_Surface>&,
                   const core_Color&,
                   const double) {} // opacity

  virtual void
    REDRAW_SURFACE(const std::string&,
                   const t_ptr<geom_Surface>&,
                   const double, // U min
                   const double, // U max
                   const double, // V min
                   const double, // V max
                   const core_Color&) {}

  virtual void
    REDRAW_SURFACE(const std::string&,
                   const t_ptr<geom_Surface>&,
                   const double, // U min
                   const double, // U max
                   const double, // V min
                   const double, // V max
                   const core_Color&,
                   const double) {} // opacity

public:

  virtual void
    DRAW_LINK(const core_XYZ&,
              const core_XYZ&,
              const core_Color&,
              const std::string&) {}

  virtual void
    DRAW_LINK(const core_UV&,
              const core_UV&,
              const core_Color&,
              const std::string&) {}

  virtual void
    REDRAW_LINK(const std::string&,
                const core_XYZ&,
                const core_XYZ&,
                const core_Color&) {}

  virtual void
    REDRAW_LINK(const std::string&,
                const core_UV&,
                const core_UV&,
                const core_Color&) {}

public:

  mobiusVisu_EXPORT virtual void
    DRAW_AXES(const core_UV&,
              const core_UV&,
              const core_UV&,
              const core_Color&,
              const std::string&);

  mobiusVisu_EXPORT virtual void
    REDRAW_AXES(const std::string&,
                const core_UV&,
                const core_UV&,
                const core_UV&,
                const core_Color&);

public:

  //! \return the scene instance.
  const t_ptr<visu_Scene>& Scene() const
  {
    return m_scene;
  }

  //! \return the commands repo.
  const t_ptr<visu_CommandRepo>& CmdRepo() const
  {
    return m_cmd;
  }

protected:

  t_ptr<visu_Scene>       m_scene;
  t_ptr<visu_CommandRepo> m_cmd;

};

}

#endif
